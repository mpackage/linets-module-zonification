<?php
namespace Linets\Zonification\Api;

use Linets\Zonification\Api\Data\DistrictInterface;
use Linets\Zonification\Model\ResourceModel\District\Collection as DistrictCollection;

/**
 * Interface DistrictRepositoryInterface
 * @api
 */
interface DistrictRepositoryInterface
{
    /**
     * @param string $id
     * @return DistrictInterface
     */
    public function get($id = null);

    /**
     * @param string $countryId
     * @param string $regionCode
     * @param string $cityCode
     * @param string $code
     * @return DistrictInterface
     */
    public function getByCodes($countryId, $regionCode, $cityCode, $code);

    /**
     * @param string $cityId
     * @return DistrictCollection
     */
    public function getDistrictsForCity($cityId);

    /**
     * @param DistrictInterface $district
     * @return DistrictInterface
     */
    public function save(DistrictInterface $district);

    /**
     * @param DistrictInterface $district
     * @return DistrictInterface
     */
    public function delete(DistrictInterface $district);
}
