<?php
namespace Linets\Zonification\Api;

use Linets\Zonification\Api\Data\CityInterface;
use Linets\Zonification\Model\ResourceModel\City\Collection as CityCollection;

/**
 * Interface CityRepositoryInterface
 * @api
 */
interface CityRepositoryInterface
{
    /**
     * @param int $id
     * @return CityInterface
     */
    public function get($id = null);

    /**
     * @param string $countryId
     * @param string $regionCode
     * @param string $code
     * @return CityInterface
     */
    public function getByCodes($countryId, $regionCode, $code);

    /**
     * @param string $regionId
     * @return CityCollection
     */
    public function getCitiesForRegion($regionId);

    /**
     * @param CityInterface $city
     * @return CityInterface
     */
    public function save(CityInterface $city);

    /**
     * @param CityInterface $city
     * @return CityInterface
     */
    public function delete(CityInterface $city);
}
