<?php
namespace Linets\Zonification\Api\Data;

interface CityInterface
{
    /**
     * city id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set city id
     *
     * @param int $cityId
     * @return $this
     */
    public function setId($cityId);

    /**
     * city id
     *
     * @return int|null
     */
    public function getCityId();

    /**
     * Set city id
     *
     * @param int $cityId
     * @return $this
     */
    public function setCityId($cityId);


    /**
     * Country id
     *
     * @return string
     */
    public function getCountryId();

    /**
     * Set Country id
     *
     * @param string $countryId
     * @return $this
     */
    public function setCountryId($countryId);

    /**
     * City region id
     *
     * @return string
     */
    public function getRegionCode();

    /**
     * Set city region id
     *
     * @param string $regionId
     * @return $this
     */
    public function setRegionCode($regionId);

    /**
     * City code
     *
     * @return int|null
     */
    public function getCode();

    /**
     * Set city code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * City name
     *
     * @return int|null
     */
    public function getName();

    /**
     * Set city name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name);
}
