<?php
namespace Linets\Zonification\Api\Data;

interface DistrictInterface
{
    /**
     * district id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set district id
     *
     * @param int $district
     * @return $this
     */
    public function setId($districtId);

    /**
     * district id
     *
     * @return int|null
     */
    public function getDistrictId();

    /**
     * Set district id
     *
     * @param int $district
     * @return $this
     */
    public function setDistrictId($districtId);


    /**
     * Country id
     *
     * @return string
     */
    public function getCountryId();

    /**
     * Set Country id
     *
     * @param string $countryId
     * @return $this
     */
    public function setCountryId($countryId);

    /**
     * City region id
     *
     * @return string
     */
    public function getRegionCode();

    /**
     * Set city region id
     *
     * @param string $regionId
     * @return $this
     */
    public function setRegionCode($regionId);

    /**
     * district region id
     *
     * @return string
     */
    public function getCityCode();

    /**
     * Set district city id
     *
     * @param string $cityId
     * @return $this
     */
    public function setCityCode($cityId);

    /**
     * district code
     *
     * @return int|null
     */
    public function getCode();

    /**
     * Set district code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * district name
     *
     * @return int|null
     */
    public function getName();

    /**
     * Set district name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name);
}
