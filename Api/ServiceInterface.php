<?php
namespace Linets\Zonification\Api;

/**
 * Interface ServiceInterface
 * @api
 */
interface ServiceInterface
{
    /**
     * @param string $regionId
     * @return string
     */
    public function getCitiesForRegion($regionId);

    /**
     * @param string $regionId
     * @return string
     */
    public function getCitiesOptionsForRegion($regionId);

    /**
     * @param string $cityId
     * @return string
     */
    public function getDistrictsForCity($cityId);

    /**
     * @param string $cityId
     * @return string
     */
    public function getDistrictsOptionsForCity($cityId);
}
