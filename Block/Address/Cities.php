<?php
namespace Linets\Zonification\Block\Address;

use Linets\Zonification\Helper\Data;

class Cities extends \Magento\Framework\View\Element\Template
{
    /**
     * @var [type]
     */
    private $helperData;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Data $helperData,
        array $data = []
    )
    {
        $this->helperData = $helperData;
        parent::__construct($context, $data);
    }

    public function citiesJson()
    {
        return $this->helperData->getCityJson();
    }
}
