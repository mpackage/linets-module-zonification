<?php
namespace Linets\Zonification\Block\Adminhtml\Order\Address;

class Form extends \Magento\Sales\Block\Adminhtml\Order\Address\Form
{
    /**
     * Return array of additional form element renderers by element id
     *
     * @return array
     */
    protected function _getAdditionalFormElementRenderers()
    {
        $additionalElements = parent::_getAdditionalFormElementRenderers();

        if ($this->directoryHelper->isZonificationActive()){
            $additionalElements['city'] = $this->getLayout()->createBlock(\Linets\Zonification\Block\Adminhtml\Edit\Renderer\City::class);
            if ($this->directoryHelper->isZonificationDistrictActive()){
                $additionalElements['district'] = $this->getLayout()->createBlock(\Linets\Zonification\Block\Adminhtml\Edit\Renderer\District::class);
            } else {
                $additionalElements['district'] = $this->getLayout()->createBlock(\Linets\Zonification\Block\Adminhtml\Edit\Renderer\EmptyElement::class);
            }
        } else {
            $additionalElements['district'] = $this->getLayout()->createBlock(\Linets\Zonification\Block\Adminhtml\Edit\Renderer\EmptyElement::class);
        }

        $additionalElements['city_id'] = $this->getLayout()->createBlock(\Linets\Zonification\Block\Adminhtml\Edit\Renderer\EmptyElement::class);
        $additionalElements['city_code'] = $this->getLayout()->createBlock(\Linets\Zonification\Block\Adminhtml\Edit\Renderer\EmptyElement::class);
        $additionalElements['district_id'] = $this->getLayout()->createBlock(\Linets\Zonification\Block\Adminhtml\Edit\Renderer\EmptyElement::class);
        $additionalElements['district_code'] = $this->getLayout()->createBlock(\Linets\Zonification\Block\Adminhtml\Edit\Renderer\EmptyElement::class);

        return $additionalElements;
    }
}
