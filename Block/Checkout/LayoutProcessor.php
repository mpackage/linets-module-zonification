<?php
namespace Linets\Zonification\Block\Checkout;

use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;
use Magento\Checkout\Helper\Data as CheckoutHelper;
use Linets\Zonification\Helper\Data as DirectoryHelper;
use Magento\Framework\UrlInterface;

class LayoutProcessor implements LayoutProcessorInterface
{
    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @var DirectoryHelper
     */
    private $directoryHelper;

    /**
     * @var CheckoutHelper
     */
    private $checkoutHelper;

    /**
     * LayoutProcessor constructor.
     * @param CheckoutHelper $checkoutHelper
     * @param DirectoryHelper $directoryHelper
     */
    public function __construct(
        CheckoutHelper $checkoutHelper,
        DirectoryHelper $directoryHelper,
        UrlInterface $urlBuilder
    )
    {
        $this->checkoutHelper = $checkoutHelper;
        $this->directoryHelper = $directoryHelper;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @param array $jsLayout
     * @return array
     */
    public function process($jsLayout)
    {
        if ($this->directoryHelper->isZonificationActive()){
            if ($this->directoryHelper->isZonificationAjaxActive()){
                if ($this->directoryHelper->isZonificationAjaxOnlyDistrictActive()){
                    $jsLayout['components']['checkoutProvider']['dictionaries']['city_id'] = $this->directoryHelper->getCityOptions();
                } else {
                    $jsLayout['components']['checkoutProvider']['dictionaries']['city_id'] = $this->directoryHelper->getCityOptionsEmpty();
                }
                if ($this->directoryHelper->isZonificationDistrictActive()){
                    $jsLayout['components']['checkoutProvider']['dictionaries']['district_id'] = $this->directoryHelper->getDistrictOptionsEmpty();
                }
            } else {
                $jsLayout['components']['checkoutProvider']['dictionaries']['city_id'] = $this->directoryHelper->getCityOptions();
                if ($this->directoryHelper->isZonificationDistrictActive()){
                    $jsLayout['components']['checkoutProvider']['dictionaries']['district_id'] = $this->directoryHelper->getDistrictOptions();
                }
            }
        }

        $jsLayout = $this->shippingFields($jsLayout);
        $jsLayout = $this->billingFields($jsLayout);

        return $jsLayout;
    }

    protected function shippingFields($jsLayout){
        $fields = $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'];
        $fields = $this->formFieldChanges($fields, 0, $jsLayout);
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'] = $fields;

        return $jsLayout;
    }

    protected function billingFields($jsLayout){
        $isDisplayBillingOnPaymentMethodAvailable = $this->checkoutHelper->isDisplayBillingOnPaymentMethodAvailable();

        if (!$isDisplayBillingOnPaymentMethodAvailable) {
            $fields = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['beforeMethods']['children']['billing-address-form']['children']['form-fields'];
            $fields['children'] = $this->formFieldChanges($fields['children'], 1, $jsLayout);

            // REPLACE CHILDREN
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['beforeMethods']['children']['billing-address-form']['children']['form-fields'] = $fields;
        } else {
            $paymentMethodRenders = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']
            ['children']['payment']['children']['payments-list']['children'];
            if (is_array($paymentMethodRenders)) {
                foreach ($paymentMethodRenders as $name => $renderer) {
                    if (isset($renderer['children']) && array_key_exists('form-fields', $renderer['children'])) {
                        $fields = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$name]['children']['form-fields']['children'];
                        $fields = $this->formFieldChanges($fields, 1, $jsLayout);
                        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$name]['children']['form-fields']['children'] = $fields;
                    }
                }
            }
        }

        return $jsLayout;
    }

    public function formFieldChanges($fields, $type = null, $jsLayout = []){

        if ($this->directoryHelper->isZonificationActive()){

            $customScope = str_replace('.city_id', '', $fields['city_id']['dataScope']);

            $city = [
                'component' => 'Linets_Zonification/js/form/element/subregion',
                'config' => [
                    'template'    => 'Linets_Zonification/form/field-loader',
                    'elementTmpl' => 'ui/form/element/select',
                    'customScope' => $customScope,
                    'customEntry' => $customScope.'.city',
                    'customName' => '${ $.parentName }.city',
                    'optionsUrl' => ($this->directoryHelper->isZonificationAjaxActive() && !$this->directoryHelper->isZonificationAjaxOnlyDistrictActive())? $this->urlBuilder->getUrl('rest/V1/zonification/city/list-options/') : false,
                    'updateComponent' => 'region_id'
                ],
                'label' => __('City'),
                'additionalClasses' => 'city_id',
                'customEntryClasses' => 'city',
                'validation' => [
                    'required-entry' => true,
                ],
                'filterBy' => [
                    'target' => '${ $.provider }:${ $.dataScopePrefix }.region_id',
                    'field' => 'region_id',
                ],
                'deps' => [
                    'checkoutProvider'
                ],
                'imports' => [
                    'update' => '${ $.parentName }.region_id:value',
                    'isDisabled' => '${ $.parentName }.region_id:disabled',
                    'visibility' => '${ $.parentName }.region_id:visible',
                    'initialOptions' => 'index = checkoutProvider:dictionaries.city_id',
                    'setOptions' => 'index = checkoutProvider:dictionaries.city_id'
                ]
            ];
            $fields['city_id'] = array_merge($fields['city_id'], $city);

            if ($this->directoryHelper->isZonificationDistrictActive()){
                $district = [
                    'component' => 'Linets_Zonification/js/form/element/subregion',
                    'config' => [
                        'template'    => 'Linets_Zonification/form/field-loader',
                        'elementTmpl' => 'ui/form/element/select',
                        'customScope' => $customScope,
                        'customEntry' => $customScope.'.district',
                        'customName' => '${ $.parentName }.district',
                        'optionsUrl' => ($this->directoryHelper->isZonificationAjaxActive())? $this->urlBuilder->getUrl('rest/V1/zonification/district/list-options/') : false,
                        'updateComponent' => 'city_id'
                    ],
                    'label' => __('District'),
                    'additionalClasses' => 'district_id',
                    'customEntryClasses' => 'district',
                    'validation' => [
                        'required-entry' => $this->directoryHelper->isZonificationDistrictRequired(),
                    ],
                    'filterBy' => [
                        'target' => '${ $.provider }:${ $.parentScope }.city_id',
                        'field' => 'city_id',
                    ],
                    'deps' => ['checkoutProvider'],
                    'imports' => [
                        'update' => '${ $.parentName }.city_id:value',
                        'isDisabled' => '${ $.parentName }.city_id:disabled',
                        'visibility' => '${ $.parentName }.city_id:visible',
                        'initialOptions' => 'index = checkoutProvider:dictionaries.district_id',
                        'setOptions' => 'index = checkoutProvider:dictionaries.district_id'
                    ]
                ];

                $fields['district_id'] = array_merge($fields['district_id'], $district);
                $fields['district_id']['validation']['required-entry'] = $this->directoryHelper->isZonificationDistrictRequired();

            } else {
                if (isset($fields['district_id'])){
                    unset($fields['district_id']);
                }
                if (isset($fields['district'])){
                    unset($fields['district']);
                }
            }
        } else {
            if (isset($fields['city_id'])){
                unset($fields['city_id']);
            }
            if (isset($fields['district_id'])){
                unset($fields['district_id']);
            }
            if (isset($fields['district'])){
                unset($fields['district']);
            }
        }

        if (isset($fields['city_code'])){
            unset($fields['city_code']);
        }
        if (isset($fields['district_code'])){
            unset($fields['district_code']);
        }

        return $fields;
    }
}
