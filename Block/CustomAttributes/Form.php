<?php
namespace Linets\Zonification\Block\CustomAttributes;

class Form extends \Magento\CustomerCustomAttributes\Block\Form
{
    /**
     * Return array of user defined attributes
     *
     * @param string
     * @return string|false
     */
    public function getCustomAttribute($attribute = false)
    {
        if ($attribute){
            $attributes = $this->getForm()->getUserAttributes();
            if (isset($attributes[$attribute])){
                return $this->getAttributeBlock($attributes[$attribute]);
            }
        }

        return false;
    }

    /**
     * Get attribute row
     *
     * @param \Magento\Eav\Model\Attribute $attribute
     * @return $block|false
     */
    public function getAttributeBlock(\Magento\Eav\Model\Attribute $attribute)
    {
        $type = $attribute->getFrontendInput();
        $block = $this->getRenderer($type);
        if ($block) {
            $block->setAttributeObject(
                $attribute
            )->setEntity(
                $this->getEntity()
            )->setFieldIdFormat(
                $this->_fieldIdFormat
            )->setFieldNameFormat(
                $this->_fieldNameFormat
            );
            return $block;
        }

        return false;
    }

    /**
     * Returns HTML class attribute value.
     * @param \Magento\Framework\DataObject\IdentityInterface $attribute
     * @return string
     */
    public function getAttributeHtmlClass($attribute)
    {
        $classes = $this->_getValidateClasses($attribute, true);
        return empty($classes) ? '' : ' ' . implode(' ', $classes);
    }

    /**
     * Return array of validate classes
     *
     * @param \Magento\Framework\DataObject\IdentityInterface $attribute
     * @param bool $withRequired
     * @return array
     */
    protected function _getValidateClasses($attribute, $withRequired = true)
    {
        $classes = [];
        if ($withRequired && $attribute->getIsRequired()) {
            $classes[] = 'required-entry';
        }
        $inputRuleClass = $this->_getInputValidateClass($attribute);
        if ($inputRuleClass) {
            $classes[] = $inputRuleClass;
        }

        $rules = $attribute->getValidateRules();
        if (!empty($rules['min_text_length'])) {
            $classes[] = 'validate-length';
            $classes[] = 'minimum-length-' . $rules['min_text_length'];
        }
        if (!empty($rules['max_text_length'])) {
            if (!in_array('validate-length', $classes)) {
                $classes[] = 'validate-length';
            }
            $classes[] = 'maximum-length-' . $rules['max_text_length'];
        }


        return $classes;
    }

    /**
     * Return validate class by attribute input validation rule
     *
     * @param \Magento\Framework\DataObject\IdentityInterface $attribute
     * @return string|false
     */
    protected function _getInputValidateClass($attribute)
    {
        $class = false;
        $validateRules = $attribute->getValidateRules();
        if (!empty($validateRules['input_validation'])) {
            switch ($validateRules['input_validation']) {
                case 'alphanumeric':
                    $class = 'validate-alphanum';
                    break;
                case 'alphanum-with-spaces':
                    $class = 'validate-alphanum-with-spaces';
                    break;
                case 'numeric':
                    $class = 'validate-digits';
                    break;
                case 'alpha':
                    $class = 'validate-alpha';
                    break;
                case 'email':
                    $class = 'validate-email';
                    break;
                case 'url':
                    $class = 'validate-url';
                    break;
                case 'date':
                    $class = 'product-custom-option datetime-picker input-text validate-date';
                    break;
            }
        }

        return $class;
    }
}
