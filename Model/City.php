<?php
namespace Linets\Zonification\Model;

use Magento\Framework\Model\AbstractModel;
use Linets\Zonification\Api\Data\CityInterface;

Class City extends AbstractModel implements CityInterface
{

    protected function _construct()
    {
        $this->_init('\Linets\Zonification\Model\ResourceModel\City');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->_getData('city_id');
    }

    /**
     * @inheritdoc
     */
    public function setId($cityId)
    {
        $this->setData('city_id', $cityId);
    }

    /**
     * @inheritdoc
     */
    public function getCityId()
    {
        return $this->_getData('city_id');
    }

    /**
     * @inheritdoc
     */
    public function setCityId($cityId)
    {
        $this->setData('city_id', $cityId);
    }

    /**
     * @inheritdoc
     */
    public function getCountryId()
    {
        return $this->_getData('country_id');
    }

    /**
     * @inheritdoc
     */
    public function setCountryId($countryId)
    {
        $this->setData('country_id', $countryId);
    }

    /**
     * @inheritdoc
     */
    public function getRegionCode()
    {
        return $this->_getData('region_code');
    }

    /**
     * @inheritdoc
     */
    public function setRegionCode($regionCode)
    {
        $this->setData('region_code', $regionCode);
    }

    /**
     * @inheritdoc
     */
    public function getCode()
    {
        return $this->_getData('code');
    }

    /**
     * @inheritdoc
     */
    public function setCode($code)
    {
        $this->setData('code', $code);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return $this->_getData('name');
    }

    /**
     * @inheritdoc
     */
    public function setName($name)
    {
        $this->setData('name', $name);
    }
}
