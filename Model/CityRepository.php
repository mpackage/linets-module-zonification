<?php
namespace Linets\Zonification\Model;


use Magento\Directory\Model\RegionFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Linets\Zonification\Api\CityRepositoryInterface;
use Linets\Zonification\Api\Data\CityInterface;
use Linets\Zonification\Api\CityInterfaceFactory;
use Linets\Zonification\Model\City as CityModel;
use Linets\Zonification\Model\ResourceModel\City as CityResourceModel;
use Linets\Zonification\Model\ResourceModel\City\Collection as CityCollection;
use Linets\Zonification\Model\ResourceModel\City\CollectionFactory as CityCollectionFactory;

class CityRepository implements CityRepositoryInterface
{
    /**
     * @var RegionFactory
     */
    protected $regionFactory;

    /**
     * @var CityInterfaceFactory
     */
    protected $cityFactory;

    /**
     * @var CityResourceModel
     */
    protected $cityResource;

    /**
     * @var CityCollectionFactory
     */
    protected $cityCollectionFactory;

    public function __construct(
        CityFactory $cityFactory,
        CityResourceModel $cityResource,
        CityCollectionFactory $cityCollectionFactory,
        RegionFactory $regionFactory
    ){
        $this->cityFactory = $cityFactory;
        $this->cityResource = $cityResource;
        $this->cityCollectionFactory = $cityCollectionFactory;
        $this->regionFactory = $regionFactory;
    }


    /**
     * @inheritdoc
     */
    public function get($id = null)
    {
        /** @var CityModel $city */
        $city = $this->cityFactory->create();
        $this->cityResource->load($city, $id);

        if (null === $city->getId()) {
            throw new NoSuchEntityException(__('City with id "%value" does not exist.', ['value' => $id]));
        }

        return $city;
    }

    /**
     * @inheritdoc
     */
    public function getByCodes($countryId, $regionCode, $code)
    {
        /** @var CityCollection $collection */
        $collection = $this->cityCollectionFactory->create();
        $collection->addCountryFilter($countryId)
            ->addRegionFilter($regionCode)
            ->addCodeFilter($code)
            ->setOrder('name', 'ASC');

        return $collection->getFirstItem();
    }

    /**
     * @inheritdoc
     */
    public function save(CityInterface $city)
    {
        try {
            $this->cityResource->save($city);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__('Could not save City'), $e);
        }
    }

    /**
     * @inheritdoc
     */
    public function delete(CityInterface $city)
    {
        try {
            $this->cityResource->delete($city);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__('Could not delete City'), $e);
        }
    }


    /**
     * @inheritdoc
     */
    public function getCitiesForRegion($regionId)
    {
        $region = $this->regionFactory->create();
        $region = $region->load($regionId);

        /** @var CityCollection $collection */
        $collection = $this->cityCollectionFactory->create();
        $collection->addCountryFilter($region->getCountryId())
            ->addRegionFilter($region->getCode())
            ->setOrder('name', 'ASC');

        return $collection;
    }
}
