<?php
namespace Linets\Zonification\Model\ResourceModel;

class District extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('directory_country_region_city_district', 'district_id');
    }
}
