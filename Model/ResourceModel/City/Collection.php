<?php
namespace Linets\Zonification\Model\ResourceModel\City;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected $_propertyMap = ['value' => 'city_id'];

    protected function _construct()
    {
        $this->_init('Linets\Zonification\Model\City', 'Linets\Zonification\Model\ResourceModel\City');
        $this->addOrder('name', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
    }


    public function addCountryFilter($countryIds)
    {
        if (!empty($countryIds)) {
            if (is_array($countryIds)) {
                $this->addFieldToFilter('main_table.country_id', ['in' => $countryIds]);
            } else {
                $this->addFieldToFilter('main_table.country_id', $countryIds);
            }
        }
        return $this;
    }

    public function addRegionFilter($regionCodes)
    {
        if (!empty($regionCodes)) {
            if (is_array($regionCodes)) {
                $this->addFieldToFilter('main_table.region_code', ['in' => $regionCodes]);
            } else {
                $this->addFieldToFilter('main_table.region_code', $regionCodes);
            }
        }
        return $this;
    }


    public function addCodeFilter($codes)
    {
        if (!empty($codes)) {
            if (is_array($codes)) {
                $this->addFieldToFilter('main_table.code', ['in' => $codes]);
            } else {
                $this->addFieldToFilter('main_table.code', $codes);
            }
        }
        return $this;
    }


    /**
     * Return attributes to map on collectios to array Options. Public to be pluginable
     *
     * @return array
     */
    public function getPropertyMap(){
        return $this->_propertyMap;
    }


    /**
     * Convert collection items to select options array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this as $item) {
            $option = [];
            foreach ($this->getPropertyMap() as $code => $field) {
                $option[$code] = $item->getData($field);
            }
            $option['label'] = $item->getName();
            $options[] = $option;
        }

        return $options;
    }

    /**
     * Convert collection items to select options array
     *
     * @return array
     */
    public function toOptionEmptyArray()
    {
        $options = [['title' => '', 'value' => '', 'label' => __('Please select a City')]];

        return $options;
    }
}
