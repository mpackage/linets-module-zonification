<?php
namespace Linets\Zonification\Model\ResourceModel\Customer\Address\Attribute\Source;

/**
 * Address region attribute backend
 *
 */
class District extends \Magento\Eav\Model\Entity\Attribute\Source\Table
{
    /**
     * @var \Linets\Zonification\Helper\Data
     */
    protected $directoryHelper;

    /**
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory
     * @param \Linets\Zonification\Model\ResourceModel\City\CollectionFactory $citiesFactory
     */
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory,
        \Linets\Zonification\Helper\Data $directoryHelper

    ) {
        $this->directoryHelper = $directoryHelper;
        parent::__construct($attrOptionCollectionFactory, $attrOptionFactory);
    }

    /**
     * Retrieve Full Option values array
     *
     * @param bool $withEmpty Add empty option to array
     * @param bool $defaultValues
     * @return array
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        if (!$this->_options) {
            if ($this->directoryHelper->isZonificationActive() && $this->directoryHelper->isZonificationDistrictActive()){
                $this->_options = $this->directoryHelper->getDistrictOptions();
            } else {
                $this->_options = [];
            }
        }
        return $this->_options;
    }
}
