<?php
namespace Linets\Zonification\Model\ResourceModel;

class City extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('directory_country_region_city', 'city_id');
    }
}
