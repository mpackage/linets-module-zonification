<?php
namespace Linets\Zonification\Model;

use Magento\Framework\Model\AbstractModel;
use Linets\Zonification\Api\Data\DistrictInterface;

Class District extends AbstractModel implements DistrictInterface
{

    protected function _construct()
    {
        $this->_init('\Linets\Zonification\Model\ResourceModel\District');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->_getData('district_id');
    }

    /**
     * @inheritdoc
     */
    public function setId($districtId)
    {
        $this->setData('district_id', $districtId);
    }

    /**
     * @inheritdoc
     */
    public function getDistrictId()
    {
        return $this->_getData('district_id');
    }

    /**
     * @inheritdoc
     */
    public function setDistrictId($districtId)
    {
        $this->setData('district_id', $districtId);
    }

    /**
     * @inheritdoc
     */
    public function getCountryId()
    {
        return $this->_getData('country_id');
    }

    /**
     * @inheritdoc
     */
    public function setCountryId($countryId)
    {
        $this->setData('country_id', $countryId);
    }

    /**
     * @inheritdoc
     */
    public function getRegionCode()
    {
        return $this->_getData('region_code');
    }

    /**
     * @inheritdoc
     */
    public function setRegionCode($regionCode)
    {
        $this->setData('region_code', $regionCode);
    }

    /**
     * @inheritdoc
     */
    public function getCityCode()
    {
        return $this->_getData('city_code');
    }

    /**
     * @inheritdoc
     */
    public function setCityCode($cityCode)
    {
        $this->setData('city_code', $cityCode);
    }

    /**
     * @inheritdoc
     */
    public function getCode()
    {
        return $this->_getData('code');
    }

    /**
     * @inheritdoc
     */
    public function setCode($code)
    {
        $this->setData('code', $code);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return $this->_getData('name');
    }

    /**
     * @inheritdoc
     */
    public function setName($name)
    {
        $this->setData('name', $name);
    }
}
