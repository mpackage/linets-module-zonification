<?php
namespace Linets\Zonification\Model;

use Linets\Zonification\Api\ServiceInterface;
use Linets\Zonification\Helper\Data as DirectoryHelper;

class Service implements ServiceInterface
{
    /**
     * @var DirectoryHelper
     */
    protected $directoryHelper;

    public function __construct(
        DirectoryHelper $directoryHelper
    ){
        $this->directoryHelper = $directoryHelper;
    }

    /**
     * @inheritdoc
     */
    public function getCitiesForRegion($regionId){
        return $this->directoryHelper->getCityJson([$regionId]);
    }

    /**
     * @inheritdoc
     */
    public function getCitiesOptionsForRegion($regionId){
        return $this->directoryHelper->getCityOptions([$regionId]);
    }

    /**
     * @inheritdoc
     */
    public function getDistrictsForCity($cityId){
        return $this->directoryHelper->getDistrictJson([$cityId]);
    }

    /**
     * @inheritdoc
     */
    public function getDistrictsOptionsForCity($cityId){
        return $this->directoryHelper->getDistrictOptions([$cityId]);
    }
}
