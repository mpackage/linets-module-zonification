<?php
namespace Linets\Zonification\Model;

use Magento\Directory\Model\RegionFactory;
use Magento\Framework\Exception\InputException;
use Linets\Zonification\Api\CityRepositoryInterface;
use Linets\Zonification\Api\DistrictRepositoryInterface;
use Linets\Zonification\Helper\Data as DirectoryHelper;

class ZonificationDataProcessor
{
    /**
     * @var CityRepositoryInterface
     */
    protected $cityRepository;

    /**
     * @var DistrictRepositoryInterface
     */
    protected $districtRepository;

    /**
     * @var RegionFactory
     */
    protected $regionFactory;

    /**
     * @var DirectoryHelper
     */
    protected $directoryHelper;

    public function __construct(
        CityRepositoryInterface $cityRepository,
        DistrictRepositoryInterface $districtRepository,
        RegionFactory $regionFactory,
        DirectoryHelper $directoryHelper
    ){
        $this->cityRepository = $cityRepository;
        $this->districtRepository = $districtRepository;
        $this->regionFactory = $regionFactory;
        $this->directoryHelper = $directoryHelper;
    }

    /**
     * Process customer custom attribute before save shipping or billing address
     *
     * @param mixed $regionId
     * @param mixed $cityId
     * @param mixed $city
     * @param mixed $districtId
     * @param mixed $district
     * @param mixed $countryId
     * @param mixed $regionCode
     * @param mixed $cityCode
     * @param mixed $districtCode
     * @return array
     */
    public function execute(
        $regionId = null,
        $cityId = null,
        $city = null,
        $districtId = null,
        $district = null,
        $countryId = null,
        $regionCode = null,
        $cityCode = null,
        $districtCode = null
    ): array {
        $resultCity = $city;
        $resultDistrict = $district;
        $resultCityId = null;
        $resultCityCode = null;
        $resultDistrictId = null;
        $resultDistrictCode = null;

        if ($this->directoryHelper->isZonificationActive() && ($regionId || $regionCode)){
            if ($cityId || $cityCode || $city){
                $regionObject = $this->regionFactory->create();
                if ($regionId){
                    $regionObject->load($regionId);
                } else {
                    $regionObject->loadByCode($regionCode, $countryId);
                }
                $regionId = $regionObject->getId();

                if ($regionId){
                    if ($cityId){
                        $cityObject = $this->cityRepository->get($cityId);
                    } else {
                        $city = strtolower(htmlspecialchars(str_replace(' ', '', $city)));
                        $cityObject = $this->cityRepository->getByCodes($countryId, $regionObject->getCode(), $cityCode ?: $city);
                    }

                    if ($cityObject->getId()){
                        if ($cityObject->getRegionCode() != $regionObject->getCode()){
                            $this->throwException(__('The city information doesn\'t match with region information.'));
                        } else {
                            $resultCityId = $cityObject->getId();
                            $resultCityCode = $cityObject->getCode();
                            $resultCity = $cityObject->getName();
                            if ($this->directoryHelper->isZonificationDistrictActive() && ($districtId || $districtCode || $district)){
                                if ($districtId){
                                    $districtObject = $this->districtRepository->get($districtId);
                                } else {
                                    $districtObject = $this->districtRepository->getByCodes($countryId, $regionObject->getCode(), $cityObject->getCode(), $districtCode ?: $district);
                                }

                                if ($districtObject->getId()){
                                    if ($districtObject->getCityCode() != $cityObject->getCode()){
                                        $this->throwException(__('The district information doesn\'t match with city information.'));
                                    } else {
                                        $resultDistrictId = $districtObject->getId();
                                        $resultDistrictCode = $districtObject->getCode();
                                        $resultDistrict = $districtObject->getName();
                                    }
                                } else {
                                    if ($this->directoryHelper->isZonificationDistrictRequired() && $this->districtRepository->getDistrictsForCity($cityObject->getId())->getSize()){
                                        $this->throwException(__('Your selected City needs a valid District Code'));
                                    }
                                }
                            }
                        }
                    } else {
                        if ($this->cityRepository->getCitiesForRegion($regionObject->getRegionId())->getSize()){
                            $this->throwException(__('Your selected region needs a valid City Code'));
                        }
                    }
                }
            }
        }

        $data = [
            'region_id' => $regionId,
            'city_id' => $resultCityId,
            'city_code' => $resultCityCode,
            'city' => $resultCity,
            'district_id' => $resultDistrictId,
            'district_code' => $resultDistrictCode,
            'district' => $resultDistrict,
        ];
        return $data;
    }

    private function throwException($message){
        $inputException = new InputException();
        $inputException->addError($message);
        throw $inputException;
    }
}
