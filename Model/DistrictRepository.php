<?php
namespace Linets\Zonification\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;

use Linets\Zonification\Api\CityInterfaceFactory;
use Linets\Zonification\Api\DistrictRepositoryInterface;
use Linets\Zonification\Api\Data\DistrictInterface;
use Linets\Zonification\Api\DistrictInterfaceFactory;
use Linets\Zonification\Model\District as DistrictModel;
use Linets\Zonification\Model\ResourceModel\District as DistrictResourceModel;
use Linets\Zonification\Model\ResourceModel\District\Collection as DistrictCollection;
use Linets\Zonification\Model\ResourceModel\District\CollectionFactory as DistrictCollectionFactory;

class DistrictRepository implements DistrictRepositoryInterface
{


    /**
     * @var CityInterfaceFactory
     */
    protected $cityFactory;

    /**
     * @var DistrictInterfaceFactory
     */
    protected $districtFactory;

    /**
     * @var DistrictResourceModel
     */
    protected $districtResource;

    /**
     * @var DistrictCollectionFactory
     */
    protected $districtCollectionFactory;

    public function __construct(
        DistrictFactory $districtFactory,
        DistrictResourceModel $districtResource,
        DistrictCollectionFactory $districtCollectionFactory,
        CityFactory $cityFactory
    ){
        $this->districtFactory = $districtFactory;
        $this->districtResource = $districtResource;
        $this->districtCollectionFactory = $districtCollectionFactory;
        $this->cityFactory = $cityFactory;
    }


    /**
     * @inheritdoc
     */
    public function get($id = null)
    {
        /** @var DistrictModel $district */
        $district = $this->districtFactory->create();
        $this->districtResource->load($district, $id);

        if (null === $district->getId()) {
            throw new NoSuchEntityException(__('District with id "%value" does not exist.', ['value' => $id]));
        }

        return $district;
    }


    /**
     * @inheritdoc
     */
    public function getByCodes($countryId, $regionCode, $cityCode, $code)
    {
        /** @var DistrictCollection $collection */
        $collection = $this->districtCollectionFactory->create();
        $collection->addCountryFilter($countryId)
            ->addRegionFilter($regionCode)
            ->addCityFilter($cityCode)
            ->addCodeFilter($code)
            ->setOrder('name', 'ASC');

        return $collection->getFirstItem();
    }



    /**
     * @inheritdoc
     */
    public function save(DistrictInterface $district)
    {
        try {
            $this->districtResource->save($district);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__('Could not save District'), $e);
        }
    }

    /**
     * @inheritdoc
     */
    public function delete(DistrictInterface $district)
    {
        try {
            $this->districtResource->delete($district);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__('Could not delete District'), $e);
        }
    }

    /**
     * @inheritdoc
     */
    public function getDistrictsForCity($cityId)
    {
        $city = $this->cityFactory->create();
        $city = $city->load($cityId);

        /** @var DistrictCollection $collection */
        $collection = $this->districtCollectionFactory->create();
        $collection->addCountryFilter($city->getCountryId())
            ->addRegionFilter($city->getRegionCode())
            ->addCityFilter($city->getCityId())
            ->setOrder('name', 'ASC');

        return $collection;
    }
}
