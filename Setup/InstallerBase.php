<?php
namespace Linets\Zonification\Setup;


use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;


class InstallerBase implements DataPatchInterface
{

    /**
     * @var \Linets\Zonification\Helper\Data
     */
    private $data;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param \Linets\Zonification\Helper\Data $data
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        \Linets\Zonification\Helper\Data $data,
        ResourceConnection $resourceConnection
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->data = $data;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        // Install Regions
        if (count($this->getRegionsData())){
            $this->addCountryRegions(
                $this->moduleDataSetup->getConnection(),
                $this->getRegionsData(),
                $this->getDataMappingForRegion()
            );
        }

        // Install Cities
        if (count($this->getCitiesData())) {
            $this->addRegionCities(
                $this->moduleDataSetup->getConnection(),
                $this->getCitiesData(),
                $this->getDataMappingForCity()
            );
        }

        // Install Districts
        if (count($this->getDistrictsData())) {
            $this->addCityDistricts(
                $this->moduleDataSetup->getConnection(),
                $this->getDistrictsData(),
                $this->getDataMappingForDistrict()
            );
        }

        $this->moduleDataSetup->endSetup();
    }

    /**
     * Map Region Data array index to model
     *
     * @return array
     */
    protected function getDataMappingForRegion(){
        return [
            'code' => 0,
            'name' => 1
        ];
    }

    /**
     * Map City Data array index to model
     *
     * @return array
     */
    protected function getDataMappingForCity(){
        return [
            'region_code' => 0,
            'code' => 1,
            'name' => 2
        ];
    }

    /**
     * Map District Data array index to model
     *
     * @return array
     */
    protected function getDataMappingForDistrict(){
        return [
            'region_code' => 0,
            'city_code' => 1,
            'code' => 2,
            'name' => 3
        ];
    }

    /**
     * States data.
     *
     * @return array
     */
    protected function getRegionsData()
    {
        /*  Country Code / Region Code / Region Name */
        // ['AR' => [ 'AR-C', 'Capital Federal']]
        return [];
    }

    /**
     * Cities data.
     *
     * @return array
     */
    protected function getCitiesData(){
        /*  Country Code / Region Code / City Code / City Name */
        // ['AR' => ['AR-C', 'caba', 'CABA']]
        return [];
    }

    /**
     * Districts data.
     *
     * @return array
     */
    protected function getDistrictsData(){
        /*  Country Code / Region Code / City Code / District Code / District Name */
        // ['AR' => ['AR-C', 'caba', 'centro', 'Microcentro']]
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [
            \Magento\Directory\Setup\Patch\Data\InitializeDirectoryData::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }






    /**
     * Add country-region data.
     *
     * @param AdapterInterface $adapter
     * @param array $data
     */
    protected function addCountryRegions(AdapterInterface $adapter, array $data, $mapper = [])
    {
        /**
         * Fill table directory/country_region
         * Fill table directory/country_region_name for en_US locale
         */
        $success = [];
        foreach ($data as $countryId => $countryRow) {
            foreach ($countryRow as $row) {
                $code = $this->clearString($row[$mapper['code']]);
                $name = $this->parseName($row[$mapper['name']]);

                if (
                    !isset($success[$countryId]) ||
                    !isset($success[$countryId][$code])
                ){
                    $bind = ['country_id' => $countryId, 'code' => $code, 'default_name' => $name];
                    $adapter->insert($this->resourceConnection->getTableName('directory_country_region'), $bind);
                    $regionId = $adapter->lastInsertId($this->resourceConnection->getTableName('directory_country_region'));
                    $bind = ['locale' => 'en_US', 'region_id' => $regionId, 'name' => $name];
                    $adapter->insert($this->resourceConnection->getTableName('directory_country_region_name'), $bind);

                    if (!isset($success[$countryId])){
                        $success[$countryId] = [];
                    }
                    if (!isset($success[$countryId][$code])){
                        $success[$countryId][$code] = true;
                    }
                }
            }
        }
        /**
         * Upgrade core_config_data general/region/state_required field.
         */
        $countries = $this->data->getCountryCollection()->getCountriesWithRequiredStates();
        $adapter->update(
            $this->resourceConnection->getTableName('core_config_data'),
            [
                'value' => implode(',', array_keys($countries))
            ],
            [
                'scope="default"',
                'scope_id=0',
                'path=?' => \Magento\Directory\Helper\Data::XML_PATH_STATES_REQUIRED
            ]
        );
    }

    /**
     * Add region-city data.
     *
     * @param AdapterInterface $adapter
     * @param array $data
     */
    protected function addRegionCities(AdapterInterface $adapter, array $data, $mapper = [])
    {
        $success = [];
        foreach ($data as $countryId => $countryRow) {
            foreach ($countryRow as $row) {
                $regionCode = $row[$mapper['region_code']];
                $code = strtolower($this->clearString($row[$mapper['code']]));
                $name = $this->parseName($row[$mapper['name']]);

                if (
                    !isset($success[$countryId]) ||
                    !isset($success[$countryId][$regionCode]) ||
                    !isset($success[$countryId][$regionCode][$code])
                ){

                    $bind = ['country_id' => $countryId, 'region_code' => $regionCode, 'code' => $this->clearString($code), 'name' => $name];
                    $adapter->insert($this->resourceConnection->getTableName('directory_country_region_city'), $bind);

                    if (!isset($success[$countryId])){
                        $success[$countryId] = [];
                    }
                    if (!isset($success[$countryId][$regionCode])){
                        $success[$countryId][$regionCode] = [];
                    }
                    if (!isset($success[$countryId][$regionCode][$code])){
                        $success[$countryId][$regionCode][$code] = true;
                    }
                }
            }
        }
    }

    /**
     * Add city-district data.
     *
     * @param AdapterInterface $adapter
     * @param array $data
     */
    protected function addCityDistricts(AdapterInterface $adapter, array $data, $mapper = [])
    {
        $success = [];
        foreach ($data as $countryId => $countryRow) {
            foreach ($countryRow as $row) {
                $regionCode = $row[$mapper['region_code']];
                $cityCode = strtolower($this->clearString($row[$mapper['city_code']]));
                $code = strtolower($this->clearString($row[$mapper['code']]));
                $name = $this->parseName($row[$mapper['name']]);

                if (
                    !isset($success[$countryId]) ||
                    !isset($success[$countryId][$regionCode]) ||
                    !isset($success[$countryId][$regionCode][$cityCode]) ||
                    !isset($success[$countryId][$regionCode][$cityCode][$code])
                ){

                    $bind = ['country_id' => $countryId, 'region_code' => $regionCode, 'city_code' => $cityCode, 'code' => $this->clearString($code), 'name' => $name];
                    $adapter->insert($this->resourceConnection->getTableName('directory_country_region_city_district'), $bind);

                    if (!isset($success[$countryId])){
                        $success[$countryId] = [];
                    }
                    if (!isset($success[$countryId][$regionCode])){
                        $success[$countryId][$regionCode] = [];
                    }
                    if (!isset($success[$countryId][$regionCode][$cityCode])){
                        $success[$countryId][$regionCode][$cityCode] = [];
                    }
                    if (!isset($success[$countryId][$regionCode][$cityCode][$code])){
                        $success[$countryId][$regionCode][$cityCode][$code] = true;
                    }
                }
            }
        }
    }

    protected function clearString($string){
        return htmlspecialchars(
            str_replace(' ', '', $string)
        );
    }

    protected function parseName($string){
        return ucfirst(strtolower($string));
    }
}
