<?php
namespace Linets\Zonification\Setup\Patch\Data;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class InstallAddressAttributesV1 implements DataPatchInterface
{

    /** @var ModuleDataSetupInterface */
    private $moduleDataSetup;

    /**
     * @var WriterInterface
     */
    protected $writerInterface;

    /**
     * @var \Magento\Customer\Setup\CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param WriterInterface $writerInterface
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        WriterInterface $writerInterface,
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->writerInterface = $writerInterface;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        /** @var \Magento\Customer\Setup\CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);

        // ========================================================================================
        // Custom attributes
        $attributesInfo = [
            'city_id' => [
                'label' => 'City',
                'type'  => 'int',
                'input' => 'select',
                'position' => 86,
                'visible' => true,
                'system' => false,
                'required' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'is_searchable_in_grid' => false,
                'source' => \Linets\Zonification\Model\ResourceModel\Customer\Address\Attribute\Source\City::class,
            ],
            'city_code' => [
                'label' => 'City Code',
                'type'  => 'varchar',
                'input' => 'text',
                'source' => '',
                'position' => 87,
                'validate_rules' => '{"max_text_length":255,"min_text_length":1}',
                'visible' => true,
                'system' => false,
                'required' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'is_searchable_in_grid' => false
            ],
            'district' => [
                'label' => 'District',
                'type'  => 'varchar',
                'input' => 'text',
                'source' => '',
                'position' => 90,
                'validate_rules' => '{"max_text_length":255,"min_text_length":1}',
                'visible' => true,
                'system' => false,
                'required' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'is_searchable_in_grid' => false
            ],
            'district_id' => [
                'label' => 'District',
                'type'  => 'int',
                'input' => 'select',
                'position' => 91,
                'visible' => true,
                'system' => false,
                'required' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'is_searchable_in_grid' => false,
                'source' => \Linets\Zonification\Model\ResourceModel\Customer\Address\Attribute\Source\District::class
            ],
            'district_code' => [
                'label' => 'District Code',
                'type'  => 'varchar',
                'input' => 'text',
                'source' => '',
                'position' => 92,
                'validate_rules' => '{"max_text_length":255,"min_text_length":1}',
                'visible' => true,
                'system' => false,
                'required' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'is_searchable_in_grid' => false
            ],
        ];

        foreach ($attributesInfo as $attributeCode => $attributeParams) {
            $customerSetup->addAttribute('customer_address', $attributeCode, $attributeParams);
            $customerSetup->addAttributeToSet('customer_address', 'Default', 'General', $attributeCode);
            $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', $attributeCode);
            $attribute->setData(
                'used_in_forms',
                ['adminhtml_customer_address','customer_address_edit', 'customer_register_address']
            );
            $attribute->setData('is_user_defined', 1);
            $attribute->isObjectNew(true);
            $attribute->save();
        }

        $customerSetup->updateAttribute('customer_address', 'city_id', 'is_user_defined', 1);
        $customerSetup->updateAttribute('customer_address', 'district_id', 'is_user_defined', 1);
        $customerSetup->updateAttribute('customer_address', 'district', 'is_user_defined', 1);
        $customerSetup->updateAttribute('customer_address', 'country_id', 'sort_order', 75);
        $customerSetup->updateAttribute('customer_address', 'region', 'sort_order', 80);
        $customerSetup->updateAttribute('customer_address', 'region_id', 'sort_order', 81);
        $customerSetup->updateAttribute('customer_address', 'city', 'sort_order', 85);
        $customerSetup->updateAttribute('customer_address', 'city_id', 'sort_order', 86);
        $customerSetup->updateAttribute('customer_address', 'district', 'sort_order', 90);
        $customerSetup->updateAttribute('customer_address', 'district_id', 'sort_order', 91);

        $this->moduleDataSetup->endSetup();
    }


    public function setConfigData($path, $value){
        $this->writerInterface->save($path, $value);
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
