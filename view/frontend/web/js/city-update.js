define([
    'jquery',
    'mage/utils/wrapper',
    'mage/template',
    'mage/validation',
    'underscore',
    'jquery/ui'
], function ($) {
    'use strict';
    return function () {
        var objCities = $eaCitiesJson,
            cityInput = $("[name*='city']").val();

            $(document).ready(function (){
                var region_id = $("[name*='region_id']").val();
                var cities = [];

                if (region_id) {
                    $.each(objCities, function (indexRegion, Cities) {
                        if (indexRegion == region_id) {
                            $.each(Cities, function (index, value) {
                                if ($.inArray(value.name, cities) == -1) {
                                    cities.push(value.name);
                                }
                            });     
                        }  
                    });
                if (cities.length){
                    var city = $("[name*='city']"),
                        selectCity = city.replaceWith("<select class='required-entry' name='city' id='city'>") + '</select>',
                        htmlSelect = '<option>Selecciona una comuna</option>',
                        options;

                    $.each(cities, function (index, value) {
                        if ( value.toUpperCase() == cityInput.toUpperCase()) {
                            options = '<option value="' + value + '" selected>' + value + '</option>';
                        } else {
                            options = '<option value="' + value + '">' + value + '</option>';
                        }

                        htmlSelect += options;
                    });

                    $('#city').append(htmlSelect);
                }
            }
            });

            $(document).on('change', "[name*='region_id']", function () {

                var region_id = $(this).val(),
                    regionName = this.name,
                    cityInputName = regionName.replace("region_id", "city"),
                    cities = [];

                if (region_id) {
                    $.each(objCities, function (indexRegion, Cities) {
                        if (indexRegion == region_id) {
                            $.each(Cities, function (index, value) {
                                if ($.inArray(value.name, cities) == -1) {
                                    cities.push(value.name);
                                }
                            });
                        }  
                    });
                    var city = $("[name*='" + cityInputName + "']");
                    if (cities.length){
                        var selectCity = city.replaceWith("<select class='required-entry' name='"+cityInputName+"' id='city'>") + '</select>',
                        htmlSelect = '<option>Selecciona una comuna</option>',
                        options;

                    $.each(cities, function (index, value) {
                        options = '<option value="' + value + '">' + value + '</option>';
                        htmlSelect += options;
                    });

                    $('#city').append(htmlSelect);
                } else {
                    city.replaceWith("<input type='text' class='input-text required-entry' name='" + cityInputName + "' id='city' aria-required='true' value='' title='Comuna'>");
                }
            }
        });
    };
});