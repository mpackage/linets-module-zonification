/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'mage/template',
    'underscore',
    'jquery-ui-modules/widget',
    'mage/validation'
], function ($, mageTemplate, _) {
    'use strict';

    $.widget('mage.zonificationUpdater', {
        cityAjax: null,
        districtAjax: null,
        isLoadingCity: false,
        isLoadingDistrict: false,
        isCityRequired: false,
        options: {
            regionTemplate:
            '<option value="<%- data.value %>" <% if (data.isSelected) { %>selected="selected"<% } %>>' +
            '<%- data.title %>' +
            '</option>',
            isRegionRequired: true,
            isCityRequired: true,
            isDistrictRequired: true,
            districtActive: true,
            isZipRequired: true,
            isCountryRequired: true,
            currentRegion: null,
            currentCity: null,
            currentDistrict: null,
            isMultipleCountriesAllowed: true,
            cityJson: {},
            districtJson: {},
            ajaxActive: true
        },

        /**
         *
         * @private
         */
        _create: function () {
            this._initCountryElement();

            this.currentRegionOption = this.options.currentRegion;
            this.currentCityOption = this.options.currentCity;
            this.currentDistrictOption = this.options.currentDistrict;
            this.regionTmpl = mageTemplate(this.options.regionTemplate);

            this._updateRegion(this.element.find('option:selected').val());

            $(this.options.regionListId).on('change', $.proxy(function (e) {
                this.setOption = false;
                this.currentRegionOption = $(e.target).val();
                this._updateCity();
            }, this));

            $(this.options.regionInputId).on('focusout', $.proxy(function () {
                this.setOption = true;
            }, this));

            $(this.options.cityListId).on('change', $.proxy(function (e) {
                this.setOption = false;
                this.currentCityOption = $(e.target).val();
                this._updateDistrict();
            }, this));

            $(this.options.cityInputId).on('focusout', $.proxy(function () {
                this.setOption = true;
            }, this));

            if (this.options.districtActive){
                $(this.options.districtListId).on('change', $.proxy(function (e) {
                    this.setOption = false;
                    this.currentCityOption = $(e.target).val();
                }, this));

                $(this.options.districtInputId).on('focusout', $.proxy(function () {
                    this.setOption = true;
                }, this));
            }
        },

        /**
         *
         * @private
         */
        _initCountryElement: function () {
            if (this.options.isMultipleCountriesAllowed) {
                this.element.parents('div.field').show();
                this.element.on('change', $.proxy(function (e) {
                    this._updateRegion($(e.target).val());
                }, this));

                if (this.options.isCountryRequired) {
                    this.element.addClass('required-entry');
                    this.element.parents('div.field').addClass('required');
                }
            } else {
                this.element.parents('div.field').hide();
            }
        },

        /**
         * Remove options from dropdown list
         *
         * @param {Object} selectElement - jQuery object for dropdown list
         * @private
         */
        _removeSelectOptions: function (selectElement) {
            selectElement.find('option').each(function (index) {
                if (index) {
                    $(this).remove();
                }
            });
        },

        /**
         * Render dropdown list
         * @param {Object} selectElement - jQuery object for dropdown list
         * @param {String} key - region code
         * @param {Object} value - region object
         * @private
         */
        _renderSelectOption: function (selectElement, key, value) {
            selectElement.append($.proxy(function () {
                var name = value.name.replace(/[!"#$%&'()*+,.\/:;<=>?@[\\\]^`{|}~]/g, '\\$&'),
                    tmplData,
                    tmpl;

                if (value.code && $(name).is('span')) {
                    key = value.code;
                    value.name = $(name).text();
                }

                tmplData = {
                    value: key,
                    title: value.name,
                    isSelected: false
                };

                if (this.options.defaultRegion === key) {
                    tmplData.isSelected = true;
                }

                tmpl = this.regionTmpl({
                    data: tmplData
                });

                return $(tmpl);
            }, this));
        },

        /**
         * Takes clearError callback function as first option
         * If no form is passed as option, look up the closest form and call clearError method.
         * @private
         */
        _clearError: function () {
            var args = ['clearError', this.options.regionListId, this.options.regionInputId, this.options.postcodeId];

            if (this.options.clearError && typeof this.options.clearError === 'function') {
                this.options.clearError.call(this);
            } else {
                if (!this.options.form) {
                    this.options.form = this.element.closest('form').length ? $(this.element.closest('form')[0]) : null;
                }

                this.options.form = $(this.options.form);

                this.options.form && this.options.form.data('validator') &&
                this.options.form.validation.apply(this.options.form, _.compact(args));

                // Clean up errors on region & zip fix
                $(this.options.regionInputId).removeClass('mage-error').parent().find('[generated]').remove();
                $(this.options.regionListId).removeClass('mage-error').parent().find('[generated]').remove();
                $(this.options.cityInputId).removeClass('mage-error').parent().find('[generated]').remove();
                $(this.options.cityListId).removeClass('mage-error').parent().find('[generated]').remove();
                $(this.options.districtInputId).removeClass('mage-error').parent().find('[generated]').remove();
                $(this.options.districtListId).removeClass('mage-error').parent().find('[generated]').remove();
                $(this.options.postcodeId).removeClass('mage-error').parent().find('[generated]').remove();
            }
        },

        /**
         * Update dropdown list based on the country selected
         *
         * @param {String} country - 2 uppercase letter for country code
         * @private
         */
        _updateRegion: function (country) {
            // Clear validation error messages
            var regionList = $(this.options.regionListId),
                regionInput = $(this.options.regionInputId),
                postcode = $(this.options.postcodeId),
                label = regionList.parent().siblings('label'),
                container = regionList.parents('div.field');

            this._clearError();
            this._checkRegionRequired(country);

            // Populate state/province dropdown list if available or use input box
            if (this.options.regionJson[country]) {
                this._removeSelectOptions(regionList);
                $.each(this.options.regionJson[country], $.proxy(function (key, value) {
                    this._renderSelectOption(regionList, key, value);
                }, this));

                if (this.currentRegionOption) {
                    regionList.val(this.currentRegionOption);
                }

                if (this.setOption) {
                    regionList.find('option').filter(function () {
                        return this.text === regionInput.val();
                    }).attr('selected', true);
                }

                if (this.options.isRegionRequired) {
                    regionList.addClass('required-entry').removeAttr('disabled');
                    container.addClass('required').show();
                } else {
                    regionList.removeClass('required-entry validate-select').removeAttr('data-validate');
                    container.removeClass('required');

                    if (!this.options.optionalRegionAllowed) { //eslint-disable-line max-depth
                        regionList.hide();
                        container.hide();
                    } else {
                        regionList.show();
                    }
                }

                regionList.show();
                regionInput.hide();
                label.attr('for', regionList.attr('id'));
            } else {
                this._removeSelectOptions(regionList);

                if (this.options.isRegionRequired) {
                    regionInput.addClass('required-entry').removeAttr('disabled');
                    container.addClass('required').show();
                } else {
                    if (!this.options.optionalRegionAllowed) { //eslint-disable-line max-depth
                        regionInput.attr('disabled', 'disabled');
                        container.hide();
                    }
                    container.removeClass('required');
                    regionInput.removeClass('required-entry');
                }

                regionList.removeClass('required-entry').prop('disabled', 'disabled').hide();
                regionInput.show();
                label.attr('for', regionInput.attr('id'));
            }

            // If country is in optionalzip list, make postcode input not required
            if (this.options.isZipRequired) {
                $.inArray(country, this.options.countriesWithOptionalZip) >= 0 ?
                    postcode.removeClass('required-entry').closest('.field').removeClass('required') :
                    postcode.addClass('required-entry').closest('.field').addClass('required');
            }

            // Add defaultvalue attribute to state/province select element
            regionList.attr('defaultvalue', this.options.defaultRegion);

            this._updateCity();
        },

        /**
         * Update dropdown list based on the region selected
         *
         * @private
         */
        _updateCity: function () {
            // Clear validation error messages
            var cityList = $(this.options.cityListId),
                cityInput = $(this.options.cityInputId),
                regionList = $(this.options.regionListId),
                label = cityList.parent().siblings('label'),
                requiredLabel = cityList.parents('div.field'),
                showCityInput = true;


            this._removeSelectOptions($(this.options.cityListId));

            if (regionList.is(':visible')){
                var cities = {};
                var selectedRegion = regionList.val();
                if (selectedRegion){
                    cities = this._getCities(selectedRegion);
                }

                if (Object.keys(cities).length || !selectedRegion){

                    //this._checkCityRequired(selectedRegion);

                    $.each(this._getSortedOptions(cities), $.proxy(function (key, value) {
                        this._renderSelectOption(cityList, value[0], value[1]);
                    }, this));

                    if (this.currentCityOption) {
                        cityList.val(this.currentCityOption);
                    }

                    if (this.setOption) {
                        cityList.find('option').filter(function () {
                            return this.text === cityInput.val();
                        }).attr('selected', true);
                    }

                    if (this.currentCityOption){
                        var $this = this;
                        cityList.find('option').filter(function () {
                            return this.value === $this.currentCityOption;
                        }).attr('selected', true);
                    }

                    cityList.removeAttr('disabled');
                    if (this.options.isCityRequired) {
                        cityList.addClass('required-entry');
                        requiredLabel.addClass('required');
                    } else {
                        cityList.removeClass('required-entry validate-select').removeAttr('data-validate');
                        requiredLabel.removeClass('required');
                    }

                    cityList.show();
                    cityInput.hide();
                    label.attr('for', cityList.attr('id'));
                    showCityInput = false;
                    if (!regionList.val()){
                        cityList.attr('disabled', 'disabled');
                    }
                }
            }

            if (showCityInput){
                if (this.options.isCityRequired) {
                    cityInput.addClass('required-entry').removeAttr('disabled');
                    requiredLabel.addClass('required');
                } else {
                    if (!this.options.optionalCityAllowed) { //eslint-disable-line max-depth
                        cityInput.attr('disabled', 'disabled');
                    }
                    requiredLabel.removeClass('required');
                    cityInput.removeClass('required-entry');
                }

                cityList.removeClass('required-entry').hide();
                cityList.val('')
                cityInput.show();
                label.attr('for', cityInput.attr('id'));
            }

            cityList.attr('defaultvalue', this.options.defaultCity);
            this.options.defaultCity = null;
            this._updateDistrict();
        },

        /**
         * Update dropdown list based on the region selected
         *
         * @private
         */
        _updateDistrict: function () {

            if (!this.options.districtActive){
                return;
            }

            // Clear validation error messages
            var districtList = $(this.options.districtListId),
                districtInput = $(this.options.districtInputId),
                cityList = $(this.options.cityListId),
                label = districtList.parent().siblings('label'),
                requiredLabel = districtList.parents('div.field'),
                showDistrictInput = true;


            this._removeSelectOptions($(this.options.districtListId));

            if (cityList.is(':visible')){
                var districts = {};
                var selectedCity = cityList.val();
                if (selectedCity){
                    districts = this._getDistricts(selectedCity);
                }

                if (Object.keys(districts).length || !selectedCity){

                    //this._checkDistrictRequired(selectedCity);

                    $.each(this._getSortedOptions(districts), $.proxy(function (key, value) {
                        this._renderSelectOption(districtList, value[0], value[1], this.options.defaultDistrict);
                    }, this));

                    if (this.currentDistrictOption) {
                        districtList.val(this.currentDistrictOption);
                    }

                    if (this.setOption) {
                        districtList.find('option').filter(function () {
                            return this.text === districtInput.val();
                        }).attr('selected', true);
                    }
                    if (this.currentDistrictOption){
                        var $this = this;
                        districtList.find('option').filter(function () {
                            return this.value === $this.currentDistrictOption;
                        }).attr('selected', true);
                    }

                    districtList.removeAttr('disabled');
                    if (this.options.isDistrictRequired) {
                        districtList.addClass('required-entry');
                        requiredLabel.addClass('required');
                    } else {
                        districtList.removeClass('required-entry validate-select').removeAttr('data-validate');
                        requiredLabel.removeClass('required');
                    }

                    districtList.show();
                    districtInput.hide();
                    label.attr('for', districtList.attr('id'));
                    showDistrictInput = false;
                    if (!cityList.val()){
                        districtList.attr('disabled', 'disabled');
                    }
                }
            }

            if (showDistrictInput){
                if (this.options.isDistrictRequired) {
                    districtInput.addClass('required-entry').removeAttr('disabled');
                    requiredLabel.addClass('required');
                } else {
                    if (!this.options.optionalDistrictAllowed) { //eslint-disable-line max-depth
                        districtInput.attr('disabled', 'disabled');
                    }
                    requiredLabel.removeClass('required');
                    districtInput.removeClass('required-entry');
                }

                districtList.removeClass('required-entry').hide();
                districtList.val('');
                districtInput.show();
                label.attr('for', districtInput.attr('id'));
            }

            districtList.attr('defaultvalue', this.options.defaultDistrict);
            this.options.defaultDistrict = null;
        },

        _getCities: function(region) {
            if(!region){
                return {};
            }

            this.isLoadingCity = false;
            if (this.options.ajaxActive){
                $(document.body).trigger('processStop');
                if (this.cityAjax !== null){
                    this.cityAjax.abort();
                    this.cityAjax = null;
                }
                if (this.districtAjax !== null){
                    this.districtAjax.abort();
                    this.districtAjax = null;
                    this.isLoadingDistrict = false;
                }
            }

            if (this.options.cityJson[region]){
                return this.options.cityJson[region];
            }

            if (this.options.ajaxActive){
                var _this = this;
                this.isLoadingCity = true;
                $(document.body).trigger('processStart');
                $.ajax({
                    url: _this.options.cityJsonURL.replace(':regionId', region),
                    type: 'GET',
                    cache: true,
                    data: '',
                    complete: function () {
                        $(document.body).trigger('processStop');
                    },
                    success: function(response){
                        _this.options.cityJson[region] = {};
                        $.extend(_this.options.cityJson, JSON.parse(response));
                        _this.isLoadingCity = false;
                        _this._updateCity();
                    }
                });
            }

            return {};
        },
        _getDistricts: function(city) {
            if(!city || !this.options.districtActive){
                return {};
            }

            this.isLoadingDistrict = false;
            if (this.options.ajaxActive) {
                $(document.body).trigger('processStop');
                if (this.districtAjax !== null) {
                    this.districtAjax.abort();
                    this.districtAjax = null;
                }
            }

            if (this.options.districtJson[city]) {
                return this.options.districtJson[city];
            }

            if (this.options.ajaxActive) {
                var _this = this;
                this.isLoadingDistrict = true;
                $(document.body).trigger('processStart');
                $.ajax({
                    url: _this.options.districtJsonURL.replace(':cityId', city),
                    type: 'GET',
                    cache: true,
                    data: '',
                    complete: function () {
                        $(document.body).trigger('processStop');
                    },
                    success: function (response) {
                        _this.options.districtJson[city] = {};
                        $.extend(_this.options.districtJson, JSON.parse(response));
                        _this.isLoadingDistrict = false;
                        _this._updateDistrict();
                    }
                });
            }

            return {};
        },

        /**
         * Check if the selected country has a mandatory region selection
         *
         * @param {String} country - Code of the country - 2 uppercase letter for country code
         * @private
         */
        _checkRegionRequired: function (country) {
            var self = this;
            this.options.isRegionRequired = false;
            $.each(this.options.regionJson.config['regions_required'], function (index, elem) {
                if (elem === country) {
                    self.options.isRegionRequired = true;
                }
            });
        },

        /**
         * Check if the selected region has a mandatory city selection
         *
         * @private
         */
        _checkCityRequired: function () {
            var self = this;

            this.options.isCityRequired = false;

            if (this.options.cityJson['config']){
                if (this.options.cityJson.config['cities_required'] && this.options.cityJson.config['cities_required'].length) {
                    self.options.isCityRequired = true;
                }
            }
        },

        /**
         * Check if the selected city has a mandatory district selection
         *
         * @private
         */
        _checkDistrictRequired: function () {
            var self = this;

            this.options.isDistrictRequired = false;

            if (!this.options.districtActive){
                return;
            }

            if (this.options.districtJson['config']){
                if (this.options.districtJson.config['districts_required'] && this.options.districtJson.config['districts_required'].length) {
                    self.options.isDistrictRequired = true;
                }
            }
        },

        _getSortedOptions: function(unSortedOptions){
            var options = Object.entries(unSortedOptions);
            options.sort(function(a,b) {
                if(a[1].name < b[1].name){
                    return -1;
                }else if(a[1].name > b[1].name){
                    return 1;
                }
                return 0;
            });
            return options;
        }
    });

    return $.mage.zonificationUpdater;
});
