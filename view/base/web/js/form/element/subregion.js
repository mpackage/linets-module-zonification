/**
 * @api
 */
define([
    'underscore',
    'ko',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'mageUtils',
    'uiLayout',
    'jquery'
], function (_, ko,registry, Select, utils,layout, $) {
    'use strict';


    return Select.extend({
        filterPending : false,
        isRequired: false,
        initialized : false,
        defaults: {
            isLoading: false,
            optionsUrl : false,
            skipValidation: false,
            ajaxOptionsLoaded : [],
            ajaxLoad : null,
            updateComponent : ''
        },

        /**
         * Extends instance with defaults, extends config with formatted values
         *     and options, and invokes initialize method of AbstractElement class.
         *     If instance's 'customEntry' property is set to true, calls 'initInput'
         */
        initialize: function () {
            if (this.initialized){
                return this;
            }

            this._super();

            this.isRequired = this.required();
            this.update();

            this.initialized = true;
            return this;
        },

        /**
         * Calls 'initObservable' of parent, initializes 'options' and 'initialOptions'
         *     properties, calls 'setOptions' passing options to it
         *
         * @returns {Object} Chainable.
         */
        initObservable: function () {
            this._super();

            this.observe('isLoading');
            this.isLoading(false);

            return this;
        },

        /**
         * Creates input from template, renders it via renderer.
         *
         * @returns {Object} Chainable.
         */
        initInput: function () {

            return this;
        },

        /**
         * @param {String} value
         */
        update: function (value) {
            if (this.skipValidation) {
                this.validation['required-entry'] = false;
                this.required(false);
            }
            this.filter();
        },

        /**
         * Extends 'additionalClasses' object.
         *
         * @returns {Abstract} Chainable.
         */
        _setClasses: function () {
            var additional = this.additionalClasses;

            if (_.isString(additional)) {
                this.additionalClasses = {};

                if (additional.trim().length) {
                    additional = additional.trim().split(' ');

                    additional.forEach(function (name) {
                        if (name.length) {
                            this.additionalClasses[name] = true;
                        }
                    }, this);
                }
            }

            _.extend(this.additionalClasses, {
                _required: this.required,
                _error: this.error,
                _warn: this.warn,
                _disabled: this.disabled,
                _loading : this.isLoading
            });

            return this;
        },

        /**
         * Filters 'initialOptions' property by 'field' and 'value' passed,
         * calls 'setOptions' passing the result to it
         *
         * @param {*} value
         * @param {String} field
         */
        defaultFilterOptions: function (value, field) {
            var source = this.initialOptions,
                result;

            field = field || this.filterBy.field;

            result = _.filter(source, function (item) {
                return item[field] === value || item.value === '';
            });

            this.setOptions(result)
        },

        setOptions : function(options){
            // PREVENT HIDE SELECT
            var $customEntry = this.customEntry;
            this.customEntry = null;
            this._super(options);
            this.customEntry = $customEntry;
        },

        filterOptions: function (value, field) {
            if (this.ajaxLoad !== null){
                this.ajaxLoad.abort();
                this.ajaxLoad = null;
            }
            if (this.optionsUrl && value && this.ajaxOptionsLoaded.indexOf(value) === -1){
                if (this.ajaxOptionsLoaded.length === 0){
                    this.initialOptions = [];
                }
                this.isLoading(true);
                this._setClasses();
                this.setOptions([]);
                this.ajaxLoad = $.ajax({
                    url: this.optionsUrl + value,
                    type: 'GET',
                    cache: false,
                    data: '',
                    success: $.proxy(function(response){
                        this.ajaxOptionsLoaded.push(value);
                        this.isLoading(false);
                        if (response){
                            var optionsToPush = this.initialOptions;
                            _.each(response, function(item){
                                optionsToPush.push(item);
                            });
                            this.initialOptions = optionsToPush;
                        }
                        this.defaultFilterOptions(value, field);
                        this.toggleInput(!this.options().length);
                    },this),
                    error: $.proxy(function(response){
                        this.defaultFilterOptions(value, field);
                        this.isLoading(false);
                    },this)
                });
            } else {
                this.defaultFilterOptions(value, field);
            }
        },


        /**
         * Change visibility for input.
         *
         * @param {Boolean} isVisible
         */
        toggleInput: function (isVisible) {
            this.disabled(isVisible);
            this.setVisible(!isVisible);


            if (this.initialized){
                this.required(isVisible? false : this.isRequired);
                this.validation['required-entry'] = isVisible? false : this.isRequired;
            }

            if (this.customName){
                registry.get(this.customName, function (input) {
                    if (this.initialized){
                        input.validation['required-entry'] = this.isRequired;
                        input.required(this.isRequired);
                    }
                    input.setVisible(isVisible);
                }.bind(this));
            }
        },

        isDisabled : function(disabled){
            if (disabled){
                this.toggleInput(false);
                this.disabled(true);
            }
        },

        visibility : function(visible){
            if (!visible){
                this.toggleInput(true);
            } else {
                registry.get(this.parentName + '.' + this.updateComponent, function (parent) {
                    if (!parent.value()) {
                        this.toggleInput(false);
                        this.disabled(true);
                    }
                }.bind(this));
            }
        },


        /**
         * Callback that fires when 'value' property is updated.
         */
        onUpdate: function () {
            this._super();
            if (this.customName) {
                registry.get(this.customName, function (input) {
                    input.value(this.getPreview());
                }.bind(this));
            }
        },

        /**
         * Filters 'initialOptions' property by 'field' and 'value' passed,
         * calls 'setOptions' passing the result to it
         *
         * @param {*} value
         * @param {String} field
         */
        filter: function (value, field) {
            if (this.filterPending){
                return true;
            }
            this.filterPending = true;
            registry.get(this.parentName + '.' + this.updateComponent, function (parent) {
                this.filterPending = false;
                if (parent) {
                    if (parent.value() && !$.isEmptyObject(parent.indexedOptions) && Object.keys(parent.indexedOptions).length){
                        this.filterOptions(parent.value(), parent.index);
                        if (!this.isLoading()){
                            this.toggleInput(!this.options().length);
                        } else {
                            this.toggleInput(false);
                            this.disabled(true);
                        }
                    } else {
                        if (!parent.value() && !$.isEmptyObject(parent.indexedOptions) && Object.keys(parent.indexedOptions).length){
                            this.toggleInput(false);
                            this.disabled(true);
                        } else {
                            this.toggleInput(true);
                        }
                    }
                }
            }.bind(this));
        }
    });
});
