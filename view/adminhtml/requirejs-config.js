var config = {
    config: {
        mixins: {
            'mage/adminhtml/form': {
                'Linets_Zonification/js/form-mixin': true
            },
            'Magento_Sales/order/edit/address/form': {
                'Linets_Zonification/order/edit/address/form-mixin': true
            }
        }
    }
};
