/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Linets_Zonification/js/form/element/subregion'
], function (Region) {
    'use strict';

    return Region.extend({
        defaults: {
            regionScope: 'data.district',
            customName: '${ $.parentName }.district',
            updateComponent: 'city_id',
        },

        /**
         * Set region to customer address form
         *
         * @param {String} value - region
         */
        setDifferedFromDefault: function (value) {
            this._super();

            if (parseFloat(value)) {
                this.source.set(this.regionScope, this.indexedOptions[value].label);
            }
        }
    });
});
