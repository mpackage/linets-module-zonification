define([
    'jquery',
    'mage/utils/wrapper',
    'prototype'
],function (jQuery, wrapper){
    'use strict';

    return function (target) {
        RegionUpdater.prototype.update = RegionUpdater.prototype.update.wrap( function ($super) {
            $super();
            if (this.regionSelectEl.disabled && this.regionSelectEl.changeUpdater){
                this.regionSelectEl.value = '';
                this.regionSelectEl.changeUpdater();
            }
        });
        RegionUpdater.prototype._checkRegionRequired = RegionUpdater.prototype._checkRegionRequired.wrap( function ($super) {
            if (typeof this.config == 'undefined') {
                return;
            }
            if (this.config['all_required']){
                if(this.config['regions_required'].indexOf(this.countryEl.value) === -1){
                    this.config['regions_required'].push(this.countryEl.value)
                }
            }
            $super();
        });
        return target;
    };
});
