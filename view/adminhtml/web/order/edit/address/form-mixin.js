define([
    'jquery',
    'mage/utils/wrapper',
],function ($, wrapper){
    'use strict';

    return function (target) {
        return function (config, element) {
            /* NEW */
            var form = $(element)
            var cityId = form.find('#city_id'),

            /**
             * Set city callback
             */
            setCity = function () {
                form.find('#city').val(cityId.filter(':visible').find(':selected').text());
            };

            if (cityId.is('visible')) {
                setCity();
            }

            cityId.on('change', setCity);
            form.find('#region_id').on('change', setCity);

            var districtId = form.find('#district_id'),

                /**
                 * Set district callback
                 */
                setDistrict = function () {
                    form.find('#district').val(districtId.filter(':visible').find(':selected').text());
                };

            if (districtId.is('visible')) {
                setDistrict();
            }

            districtId.on('change', setDistrict);
            form.find('#city_id').on('change', setDistrict);
        };
    };
});
