<?php
declare(strict_types=1);

namespace Linets\Zonification\Plugin;

use Magento\CustomerCustomAttributes\Model\CustomerAddressCustomAttributesProcessor;
use Magento\Quote\Api\Data\AddressInterface;
use Linets\Zonification\Model\ZonificationDataProcessor;

/**
 * Plugin for Set valid City and District
 */
class CustomerAddressCustomAttributesProcessorPlugin
{
    /**
     * @var ZonificationDataProcessor
     */
    protected $zonificationDataProcessor;

    public function __construct(
        ZonificationDataProcessor $zonificationDataProcessor
    ){
        $this->zonificationDataProcessor = $zonificationDataProcessor;
    }

    /**
     * Validate and set City and District
     *
     * @param CustomerAddressCustomAttributesProcessor $subject
     * @param AddressInterface $addressInformation
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterExecute(
        CustomerAddressCustomAttributesProcessor $subject,
        $result,
        AddressInterface $addressInformation
    ) {
        $isStorePickupAddress = ($addressInformation->getExtensionAttributes() && $addressInformation->getExtensionAttributes()->getPickupLocationCode());
        if (!$isStorePickupAddress){
            $cityIdAttribute = $addressInformation->getCustomAttribute('city_id');
            $cityCodeAttribute = $addressInformation->getCustomAttribute('city_code');
            $districtIdAttribute = $addressInformation->getCustomAttribute('district_id');
            $districtCodeAttribute = $addressInformation->getCustomAttribute('district_code');
            $districtAttribute = $addressInformation->getCustomAttribute('district');

            $regionId = $addressInformation->getRegionId();
            $cityId = ($cityIdAttribute)? $cityIdAttribute->getValue() : null;
            $city = $addressInformation->getCity();
            $districtId = ($districtIdAttribute)? $districtIdAttribute->getValue() : null;
            $district = ($districtAttribute)? $districtAttribute->getValue() : null;

            $processedData = $this->zonificationDataProcessor->execute(
                $regionId,
                $cityId,
                $city,
                $districtId,
                $district
            );

            $addressInformation->setCity($processedData['city']);
            if ($cityIdAttribute){
                $cityIdAttribute->setValue($processedData['city_id']);

                if ($cityCodeAttribute){
                    $cityCodeAttribute->setValue($processedData['city_code']);
                } else {
                    $addressInformation->setCustomAttribute('city_code', $processedData['city_code']);
                }
            }

            if ($districtIdAttribute){
                $districtIdAttribute->setValue($processedData['district_id']);
                if ($districtCodeAttribute){
                    $districtCodeAttribute->setValue($processedData['district_code']);
                } else {
                    $addressInformation->setCustomAttribute('district_code', $processedData['district_code']);
                }
            }
            if ($districtAttribute){
                $districtAttribute->setValue($processedData['district']);
            }
        }
    }
}
