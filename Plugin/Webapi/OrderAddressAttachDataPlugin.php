<?php
declare(strict_types=1);

namespace Linets\Zonification\Plugin\Webapi;

use Magento\CustomerCustomAttributes\Model\Sales\Order\Address;
use Magento\Sales\Api\Data\OrderAddressExtensionInterfaceFactory as ExtensionAttributeFactory;
use Linets\Zonification\Helper\Data as DirectoryHelper;

/**
 * Plugin for Set Extension attributes to api
 */
class OrderAddressAttachDataPlugin
{


    /**
     * @var DirectoryHelper
     */
    protected $directoryHelper;

    /**
     * @var ExtensionAttributeFactory
     */
    protected $extensionAttributeFactory;

    public function __construct(
        ExtensionAttributeFactory $extensionAttributeFactory,
        DirectoryHelper $directoryHelper
    ){
        $this->extensionAttributeFactory = $extensionAttributeFactory;
        $this->directoryHelper = $directoryHelper;
    }

    /**
     * Set custom attribute interface values for custom attributes
     *
     * @param Address $address
     * @param array $entities
     * @return Address
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterAttachDataToEntities(
        Address $subject,
        $address,
        $entities
    ) {
        if ($this->directoryHelper->isZonificationActive()){
            foreach ($entities as $entity){
                $extensionAttributes = $entity->getExtensionAttributes();
                if (!$extensionAttributes){
                    $extensionAttributes = $this->extensionAttributeFactory->create();
                }

                $extensionAttributes->setCityCode($entity->getCityCode());
                $extensionAttributes->setDistrictCode($entity->getDistrictCode());
                $extensionAttributes->setDistrict($entity->getDistrict());
                $entity->setExtensionAttributes($extensionAttributes);
            }
        }

        return $address;
    }
}
