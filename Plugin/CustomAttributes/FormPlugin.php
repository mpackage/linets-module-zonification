<?php
namespace Linets\Zonification\Plugin\CustomAttributes;

class FormPlugin
{
    /**
     * @param \Magento\CustomerCustomAttributes\Block\Form $subject
     * @param array $customerData
     * @return array
     */
    public function afterGetUserDefinedAttributes(
        \Magento\CustomerCustomAttributes\Block\Form $subject,
        array $attributes
    ) {
        if (isset($attributes['city_id'])){
            unset($attributes['city_id']);
        }
        if (isset($attributes['city_code'])){
            unset($attributes['city_code']);
        }
        if (isset($attributes['district_id'])){
            unset($attributes['district_id']);
        }
        if (isset($attributes['district_code'])){
            unset($attributes['district_code']);
        }
        if (isset($attributes['district'])){
            unset($attributes['district']);
        }

        return $attributes;
    }
}
