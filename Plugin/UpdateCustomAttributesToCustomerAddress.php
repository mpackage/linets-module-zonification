<?php
declare(strict_types=1);

namespace Linets\Zonification\Plugin;

use Magento\Customer\Api\Data\AddressInterface;
use Magento\Customer\Api\Data\RegionInterface;
use Magento\Customer\Model\Address;
use Linets\Zonification\Model\ZonificationDataProcessor;

/**
 * Plugin for Set valid City and District
 */
class UpdateCustomAttributesToCustomerAddress
{
    /**
     * @var ZonificationDataProcessor
     */
    protected $zonificationDataProcessor;

    public function __construct(
        ZonificationDataProcessor $zonificationDataProcessor
    ){
        $this->zonificationDataProcessor = $zonificationDataProcessor;
    }

    /**
     * Set custom attribute interface values for custom attributes
     *
     * @param Address $subject
     * @param AddressInterface $customerAddress
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeUpdateData(
        Address $subject,
        AddressInterface $customerAddress
    ) : array {
        $cityIdAttribute = $customerAddress->getCustomAttribute('city_id');
        $cityCodeAttribute = $customerAddress->getCustomAttribute('city_code');
        $districtAttribute = $customerAddress->getCustomAttribute('district');
        $districtIdAttribute = $customerAddress->getCustomAttribute('district_id');
        $districtCodeAttribute = $customerAddress->getCustomAttribute('district_code');

        $regionId = false;
        $regionCode = false;
        if ($customerAddress->getRegionId() || ($customerAddress->getRegion() instanceof RegionInterface && $customerAddress->getRegion()->getRegionId())){
            $regionId = ($customerAddress->getRegionId()) ?: $customerAddress->getRegion()->getRegionId();
        } else {
            if ($customerAddress->getRegion() instanceof RegionInterface && $customerAddress->getRegion()->getRegionCode()){
                $regionCode = $customerAddress->getRegion()->getRegionCode();
            }
        }

        $cityId = ($cityIdAttribute)? $cityIdAttribute->getValue() : null;
        $cityCode = ($cityCodeAttribute)? $cityCodeAttribute->getValue() : null;
        $city = $customerAddress->getCity();
        $districtId = ($districtIdAttribute)? $districtIdAttribute->getValue() : null;
        $districtCode = ($districtCodeAttribute)? $districtCodeAttribute->getValue() : null;
        $district = ($districtAttribute)? $districtAttribute->getValue() : null;

        $processedData = $this->zonificationDataProcessor->execute(
            $regionId,
            $cityId,
            $city,
            $districtId,
            $district,
            $customerAddress->getCountryId(),
            $regionCode,
            $cityCode,
            $districtCode
        );

        if ($regionCode && !$regionId && $processedData['region_id']){
            $customerAddress->setRegionId($processedData['region_id']);
            $customerAddress->getRegion()->setRegionId($processedData['region_id']);
        }

        $customerAddress->setCity($processedData['city']);
        if ($processedData['city_id']){
            if ($cityIdAttribute){
                $cityIdAttribute->setValue($processedData['city_id']);
            } else {
                $customerAddress->setCustomAttribute('city_id', $processedData['city_id']);
            }
            if ($cityCodeAttribute){
                $cityCodeAttribute->setValue($processedData['city_code']);
            } else {
                if ($processedData['city_code']) {
                    $customerAddress->setCustomAttribute('city_code', $processedData['city_code']);
                }
            }
        }

        if ($processedData['district_id']){
            if ($districtIdAttribute){
                $districtIdAttribute->setValue($processedData['district_id']);
            } else {
                if ($processedData['district_id']) {
                    $customerAddress->setCustomAttribute('district_id', $processedData['district_id']);
                }
            }
            if ($districtCodeAttribute){
                $districtCodeAttribute->setValue($processedData['district_code']);
            } else {
                if ($processedData['district_code']) {
                    $customerAddress->setCustomAttribute('district_code', $processedData['district_code']);
                }
            }
        }

        if ($districtAttribute){
            $districtAttribute->setValue($processedData['district']);
        } else {
            if ($processedData['district']){
                $customerAddress->setCustomAttribute('district', $processedData['district']);
            }
        }

        return [$customerAddress];
    }
}
