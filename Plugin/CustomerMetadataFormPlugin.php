<?php
declare(strict_types=1);

namespace Linets\Zonification\Plugin;

use Linets\Zonification\Model\ZonificationDataProcessor;

/**
 * Plugin for Set valid City and District
 */
class CustomerMetadataFormPlugin
{
    /**
     * @var ZonificationDataProcessor
     */
    protected $zonificationDataProcessor;

    public function __construct(
        ZonificationDataProcessor $zonificationDataProcessor
    ){
        $this->zonificationDataProcessor = $zonificationDataProcessor;
    }

    /**
     * Set custom attribute interface values for custom attributes
     *
     * @param \Magento\Customer\Model\Metadata\Form $subject
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterExtractData(
        \Magento\Customer\Model\Metadata\Form $subject,
        $data
    ) : array {
        $regionId = $data['region_id'] ?? null;
        $cityId = $data['city_id'] ?? null;
        $city = $data['city'] ?? null;
        $districtId = $data['district_id'] ?? null;
        $district = $data['district'] ?? null;

        $processedData = $this->zonificationDataProcessor->execute(
            $regionId,
            $cityId,
            $city,
            $districtId,
            $district
        );

        if (isset($data['city'])){
            $data['city'] = $processedData['city'];
        }
        if (isset($data['city_id'])){
            $data['city_id'] = $processedData['city_id'];
            $data['city_code'] = $processedData['city_code'];
        }

        if (isset($data['district_id'])){
            $data['district_id'] = $processedData['district_id'];
            $data['district_code'] = $processedData['district_code'];
        }

        if (isset($data['district'])){
            $data['district'] = $processedData['district'];
        }

        return $data;
    }
}
