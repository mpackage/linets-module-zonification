<?php
namespace Linets\Zonification\Helper;

use Magento\Directory\Model\AllowedCountries;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Directory\Model\ResourceModel\Country\Collection;
use Magento\Directory\Model\ResourceModel\Region\CollectionFactory;
use Magento\Framework\App\Cache\Type\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Json\Helper\Data as JsonData;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;
use \Magento\Directory\Model\ResourceModel\Region\Collection as RegionCollection;
use Linets\Zonification\Model\ResourceModel\City\Collection as CityCollection;
use Linets\Zonification\Model\ResourceModel\City\CollectionFactory as CityCollectionFactory;
use Linets\Zonification\Model\ResourceModel\District\Collection as DistrictCollection;
use Linets\Zonification\Model\ResourceModel\District\CollectionFactory as DistrictCollectionFactory;

class Data extends \Magento\Directory\Helper\Data
{

    const XML_PATH_REGISTER_ACTIVE = 'zonification/address/register_active';
    const XML_PATH_ZONIFICATION_ACTIVE = 'zonification/address/zonification_active';
    const XML_PATH_ZONIFICATION_AJAX = 'zonification/address/zonification_ajax';
    const XML_PATH_ZONIFICATION_AJAX_ONLY_DISTRICT = 'zonification/address/zonification_only_district_ajax';
    const XML_PATH_ZONIFICATION_DISTRICT = 'zonification/address/zonification_district';
    const XML_PATH_ZONIFICATION_DISTRICT_REQUIRED = 'zonification/address/zonification_district_required';

    const SERVICE_URL_CITY = 'rest/V1/zonification/city/list/:regionId';
    const SERVICE_URL_CITY_OPTIONS = 'rest/V1/zonification/city/list-options/:regionId';
    const SERVICE_URL_DISTRICT = 'rest/V1/zonification/district/list/:cityId';
    const SERVICE_URL_DISTRICT_OPTIONS = 'rest/V1/zonification/district/list-options/:cityId';
    /**
     * Region collection
     *
     * @var \Magento\Directory\Model\ResourceModel\Region\Collection
     */
    protected $_allRegionCollection;

    /**
     * @var CityCollectionFactory
     */
    protected $_cityCollectionFactory;

    /**
     * @var CityCollection
     */
    protected $_cityCollection;

    protected $_cityJson;

    /**
     * @var DistrictCollectionFactory
     */
    protected $_districtCollectionFactory;

    /**
     * @var DistrictCollection
     */
    protected $_districtCollection;

    protected $_districtJson;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param Config $configCacheType
     * @param Collection $countryCollection
     * @param CollectionFactory $regCollectionFactory
     * @param JsonData $jsonHelper
     * @param StoreManagerInterface $storeManager
     * @param CurrencyFactory $currencyFactory
     * @param CityCollectionFactory $cityCollectionFactory
     * @param DistrictCollectionFactory $districtCollectionFactory
     */
    public function __construct(
        Context $context,
        Config $configCacheType,
        Collection $countryCollection,
        CollectionFactory $regCollectionFactory,
        JsonData $jsonHelper,
        StoreManagerInterface $storeManager,
        CurrencyFactory $currencyFactory,
        CityCollectionFactory $cityCollectionFactory,
        DistrictCollectionFactory $districtCollectionFactory
    ) {
        parent::__construct(
            $context,
            $configCacheType,
            $countryCollection,
            $regCollectionFactory,
            $jsonHelper,
            $storeManager,
            $currencyFactory
        );

        $this->_cityCollectionFactory = $cityCollectionFactory;
        $this->_districtCollectionFactory = $districtCollectionFactory;
    }

    /**
     * Retrieve all regions collection
     *
     * @return RegionCollection
     */
    public function getAllRegionsCollection()
    {
        if (!$this->_allRegionCollection || !$this->_allRegionCollection->isLoaded()) {
            $this->_allRegionCollection = $this->_regCollectionFactory->create();

            $scope = $this->getCurrentScope();
            $allowedCountries = $this->scopeConfig->getValue(
                AllowedCountries::ALLOWED_COUNTRIES_PATH,
                $scope['type'],
                $scope['value']
            );
            $countryIds = explode(',', $allowedCountries);

            $this->_allRegionCollection->addCountryFilter($countryIds)->load();
        }

        return $this->_allRegionCollection;
    }

    /**
     * Retrieve regions collection
     *
     * @param array $regionIds
     * @return RegionCollection
     */
    public function getRegionCollectionById($regionIds){
        $regionCollection = $this->_regCollectionFactory->create()->addFieldToFilter('main_table.region_id', ['in' => $regionIds])->load();
        return $regionCollection;
    }

    /**
     * Retrieve City collection
     *
     * @param array $cityIds
     * @return RegionCollection
     */
    public function getCityCollectionById($cityIds){
        $cityCollection = $this->_cityCollectionFactory->create()->addFieldToFilter('main_table.city_id', ['in' => $cityIds])->load();
        return $cityCollection;
    }

    /**
     * Retrieve city collection
     * @param mixed $regions
     * @return CityCollection
     */
    public function getCityCollection($regions = null)
    {
        $cityCollection = null;

        if ($regions){
            if (is_array($regions)) {
                $regionCollection = $this->getRegionCollectionById($regions);
            } else {
                if ($regions instanceof \Magento\Directory\Model\Region){
                    $regionCollection = [$regions];
                } else {
                    $regionCollection = $regions;
                }
            }

            $countryIds = [];
            $regionCodes = [];
            foreach($regionCollection as $region){
                $countryIds[$region->getCountryId()] = true;
                $regionCodes[$region->getCode()] = true;
            }

            $countryIds = array_keys($countryIds);
            $regionCodes = array_keys($regionCodes);

            if (count($countryIds) && count($regionCodes)){
                $cityCollection = $this->_cityCollectionFactory->create();
                $cityCollection->addCountryFilter($countryIds)
                    ->addRegionFilter($regionCodes);
            }
        }

        return $cityCollection;
    }

    /**
     * Retrieve district collection
     * @param mixed $cities
     * @return DistrictCollection|null
     */
    public function getDistrictsCollection($cities = null)
    {
        $districtCollection = null;
        if ($cities){
            if (is_array($cities)){
                $citiesCollection = $this->getCityCollectionById($cities);
            } else {
                $citiesCollection = [$cities];
            }

            $countryIds = [];
            $regionCodes = [];
            $cityCodes = [];
            foreach($citiesCollection as $city){
                $countryIds[$city->getCountryId()] = true;
                $regionCodes[$city->getRegionCode()] = true;
                $cityCodes[$city->getCode()] = true;
            }

            $countryIds = array_keys($countryIds);
            $regionCodes = array_keys($regionCodes);
            $cityCodes = array_keys($cityCodes);

            if (count($countryIds) && count($regionCodes) && count($cityCodes)){
                $districtCollection = $this->_districtCollectionFactory->create();
                $districtCollection
                    ->addCountryFilter($countryIds)
                    ->addRegionFilter($regionCodes)
                    ->addCityFilter($cityCodes);
            }
        }


        return $districtCollection;
    }

    /**
     * Retrieve regions data
     *
     * @return array
     */
    public function getRegionData()
    {
        $collection = $this->getAllRegionsCollection();
        $regions = [
            'config' => [
                'show_all_regions' => $this->isShowNonRequiredState(),
                'regions_required' => $this->getCountriesWithStatesRequired(),
            ],
        ];
        foreach ($collection as $region) {
            /** @var $region \Magento\Directory\Model\Region */
            if (!$region->getRegionId()) {
                continue;
            }
            $regions[$region->getCountryId()][$region->getRegionId()] = [
                'code' => $region->getCode(),
                'name' => (string)__($region->getName()),
            ];
        }
        return $regions;
    }

    /**
     * Retrieve regions data
     * @param mixed $regionIds
     * @return array
     */
    public function getCityData($regionIds = null)
    {
        $cities = [
            'config' => [
                'regions_required' => [],
                'all_required' => true,
            ],
        ];

        if (is_array($regionIds)) {
            $regions = $this->getRegionCollectionById($regionIds);
        } else {
            $regions = $this->getAllRegionsCollection();
        }

        /** @var $region \Magento\Directory\Model\Region */
        foreach ($regions as $region){

            $collection = $this->getCityCollection($region);
            if ($collection){
                foreach ($collection as $city) {
                    /** @var $region \Linets\Zonification\Model\City */
                    if (!$city->getId()) {
                        continue;
                    }
                    $cities[$region->getRegionId()][$city->getCityId()] = [
                        'code' => $city->getCode(),
                        'name' => (string)__($city->getName()),
                    ];
                }
            }
        }

        return $cities;
    }

    /**
     * Retrieve City data
     *
     * @param mixed $regionIds
     * @return array
     */
    public function getCityOptions($regionIds = null)
    {
        if (is_array($regionIds)) {
            $regions = $this->getRegionCollectionById($regionIds);
        } else {
            $regions = $this->getAllRegionsCollection();
        }

        $options = [];
        foreach ($regions as $region){

            $cityCollection = $this->getCityCollection($region);
            if ($cityCollection) {
                $cityOptions = $cityCollection->toOptionArray();
                foreach ($cityOptions as $districtKey => &$cityOption) {
                    $cityOption['region_id'] = $region->getId();
                }
                $options = array_merge($options, $cityOptions);
            }
        }

        if (count($options) > 0) {
            array_unshift(
                $options,
                ['value' => '', 'label' => __('Please select a City')]
            );
        }

        return $options;
    }

    /**
     * Retrieve City Empty data
     *
     * @return array
     */
    public function getCityOptionsEmpty()
    {
        return $this->_cityCollectionFactory->create()->toOptionEmptyArray();
    }

    /**
     * Retrieve District data
     *
     * @param mixed $cityIds
     * @return array
     */
    public function getDistrictData($cityIds = null)
    {
        $districts = [
            'config' => [
                'regions_required' => [],
                'all_required' => $this->isZonificationDistrictRequired()
            ],
        ];


        if ($cityIds && is_array($cityIds)){
            $regionCollection = $this->getRegionCollectionById($cityIds);
        } else {
            $regionCollection = $this->getAllRegionsCollection();
        }

        /** @var $region \Magento\Directory\Model\Region */
        foreach ($regionCollection as $region){

            $cityCollection = $this->getCityCollection($region);
            if ($cityCollection){

                /** @var $region \Linets\Zonification\Model\City */
                foreach ($cityCollection as $city) {

                    $districtCollection = $this->getDistrictsCollection($city);
                    if ($districtCollection){

                        /** @var $region \Linets\Zonification\Model\District */
                        foreach ($districtCollection as $district){

                            if (!$district->getDistrictId()) {
                                continue;
                            }
                            $districts[$city->getCityId()][$district->getDistrictId()] = [
                                'code' => $district->getCode(),
                                'name' => (string)__($district->getName()),
                            ];
                        }
                    }
                }
            }
        }

        return $districts;
    }

    /**
     * Retrieve District data as options
     *
     * @param mixed $cities
     * @return array
     */
    public function getDistrictOptions($cities = null)
    {
        if (!is_array($cities)) {
            $regions = $this->getAllRegionsCollection();
            $cities = $this->getCityCollection($regions);
        }

        $options = [];
        if ($cities){
            foreach ($cities as $city){
                $collection = $this->getDistrictsCollection($city);
                if ($collection){
                    $districtOptions = $collection->toOptionArray();
                    foreach($districtOptions as $districtKey => &$districtOption){
                        $districtOption['city_id'] = $city->getId();
                    }
                    $options = array_merge($options, $districtOptions);
                }
            }
        }

        if (count($options) > 0) {
            array_unshift(
                $options,
                ['value' => '', 'label' => __('Please select a District')]
            );
        }

        return $options;
    }


    /**
     * Retrieve City Empty data
     *
     * @return array
     */
    public function getDistrictOptionsEmpty()
    {
        return $this->_districtCollectionFactory->create()->toOptionEmptyArray();
    }


    /**
     * Retrieve Cities data json
     * @param mixed $regionIds
     * @return string
     */
    public function getCityJson($regionIds = null)
    {
        \Magento\Framework\Profiler::start('TEST: ' . __METHOD__, ['group' => 'TEST', 'method' => __METHOD__]);
        if (!$this->_cityJson) {
            $scope = $this->getCurrentScope();
            $scopeKey = $scope['value'] ? '_' . implode('_', $scope) : null;
            if (is_array($regionIds)){
                $scopeKey .= '_regions_'.implode('_', $regionIds);
            }
            $cacheKey = 'ZONIFICATION_REGIONS_CITIES_JSON_STORE' . $scopeKey;
            $json = $this->_configCacheType->load($cacheKey);
            if (empty($json)) {
                $cities = $this->getCityData($regionIds);
                $json = $this->jsonHelper->jsonEncode($cities);
                if ($json === false) {
                    $json = 'false';
                }
                $this->_configCacheType->save($json, $cacheKey);
            }
            $this->_cityJson = $json;
        }
        \Magento\Framework\Profiler::stop('TEST: ' . __METHOD__);

        return $this->_cityJson;
    }

    /**
     * Retrieve districts data json
     * @param mixed $districtIds
     * @return string
     */
    public function getDistrictJson($cityIds = null)
    {
        \Magento\Framework\Profiler::start('TEST: ' . __METHOD__, ['group' => 'TEST', 'method' => __METHOD__]);
        if (!$this->_districtJson) {
            $scope = $this->getCurrentScope();
            $scopeKey = $scope['value'] ? '_' . implode('_', $scope) : null;
            if (is_array($cityIds)){
                $scopeKey .= '_cities_'.implode('_', $cityIds);
            }
            $cacheKey = 'ZONIFICATION_REGIONS_CITIES_DISTRICTS_JSON_STORE' . $scopeKey;
            $json = $this->_configCacheType->load($cacheKey);
            if (empty($json)) {
                $districts = $this->getDistrictData($cityIds);
                $json = $this->jsonHelper->jsonEncode($districts);
                if ($json === false) {
                    $json = 'false';
                }
                $this->_configCacheType->save($json, $cacheKey);
            }
            $this->_districtJson = $json;
        }
        \Magento\Framework\Profiler::stop('TEST: ' . __METHOD__);

        return $this->_districtJson;
    }


    /**
     * Get current scope from request
     *
     * @return array
     */
    private function getCurrentScope(): array
    {
        $scope = [
            'type' => ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            'value' => null,
        ];
        $request = $this->_getRequest();
        if ($request->getParam(ScopeInterface::SCOPE_WEBSITE)) {
            $scope = [
                'type' => ScopeInterface::SCOPE_WEBSITE,
                'value' => $request->getParam(ScopeInterface::SCOPE_WEBSITE),
            ];
        } elseif ($request->getParam(ScopeInterface::SCOPE_STORE)) {
            $scope = [
                'type' => ScopeInterface::SCOPE_STORE,
                'value' => $request->getParam(ScopeInterface::SCOPE_STORE),
            ];
        }

        return $scope;
    }

    /**
     * Return, Zonification Active
     *
     * @return bool
     */
    public function isZonificationActive()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ZONIFICATION_ACTIVE,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Return, Zonification District Active
     *
     * @return bool
     */
    public function isZonificationDistrictActive()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ZONIFICATION_DISTRICT,
            ScopeInterface::SCOPE_STORE
        );
    }


    /**
     * Return, Zonification District Active
     *
     * @return bool
     */
    public function isZonificationDistrictRequired()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ZONIFICATION_DISTRICT_REQUIRED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Return, Zonification Ajax Active
     *
     * @return bool
     */
    public function isZonificationAjaxActive()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ZONIFICATION_AJAX,
            ScopeInterface::SCOPE_STORE
        );
    }
    /**
     * Return, Zonification Ajax Active
     *
     * @return bool
     */
    public function isZonificationAjaxOnlyDistrictActive()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ZONIFICATION_AJAX_ONLY_DISTRICT,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Return Show Address Form on Register Page
     *
     * @return bool
     */
    public function useAddressOnRegister()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_REGISTER_ACTIVE,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function getServiceUrlCity(){
        return $this->_urlBuilder->getUrl() . self::SERVICE_URL_CITY;
    }
    public function getServiceUrlCityOptions(){
        return $this->_urlBuilder->getUrl() . self::SERVICE_URL_CITY_OPTIONS;
    }
    public function getServiceUrlDistrict(){
        return $this->_urlBuilder->getUrl() . self::SERVICE_URL_DISTRICT;
    }
    public function getServiceUrlDistrictOptions(){
        return $this->_urlBuilder->getUrl() . self::SERVICE_URL_DISTRICT_OPTIONS;
    }
}
